#ifndef _TCTReflect
#define _TCTReflect

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TCTReflect                                                           //
//                                                                      //
// Class managing reflections due to impedance missmatch                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "TH1F.h"

class  TCTReflect  {

private:
 public:
  Float_t RefC;
  Float_t RefTs;
  Float_t RefTe;
  TH1F *SubHis;
  Int_t SubHisCopy;

  TCTReflect(Float_t=-1,Float_t=-1,Float_t=-1);
  TH1F *CorrReflCopy(TH1F *his);
  TH1F *SubRefWF(TH1F *,Int_t=1);
  void CorrRefl(TH1F *);
  void SetSubRefWF(TH1F *);

  ~TCTReflect(){};

ClassDef(TCTReflect,1) 
};


#endif
