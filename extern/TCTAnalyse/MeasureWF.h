//#define RHDB
#ifndef _MeasureWF
#define _MeasureWF

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Meassure                                                             //
//                                                                      //
// Managing Measured WaveForms                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
//#include <Riostream.h>
#include "TArray.h"
#include "TArrayI.h"
#include "TArrayF.h"
#include "TGraph.h"
#include <stdio.h>
#include <stdlib.h>
#include "TH1F.h"
#include "TClonesArray.h"
#include "TPaveText.h"
#include "TText.h"
#include "Elec.h"
#ifdef RHDB
#include "D:\Users\Gregor\Delo\HHRHDB\MySqlHHRHDB.h"
#endif

class  MeasureWF : public TObject {
private:
  
  Int_t          Multiple;      //Number of Voltages
  Float_t 	 Frequency;	//Frequency of trigger
  TClonesArray   *histo;        //-> 
  TArrayF        Voltages;      //Array of Voltages  
  TArrayF	 Current; 	//Array of Currents
  TArrayF	 Temperature;	//Array of Temperatures
  TArrayF 	 Date;		//Date of the Measurement (or simulation)
  TArrayF        Frequencies;   //Array of Frequnecies
  Float_t        AnnealTime;
  void ReadTCTForm(TString, Int_t,Float_t=-1111, Float_t=-1111, Int_t=0); 
 public:
  TPaveText *pt;                //!
  Double_t        CAP;           //! Capacitance of the diode 
  TString        Header;        //Header of the file

  //stuff added to patch position sensitive TCT
  bool DrawMode;
  Char_t suffix[10];
  Char_t prefix[10];

#ifdef RHDB
  HHRHDBResults     *res;       //!
  DBGetString(Char_t *,Char_t *,Char_t *);
  void DBResult(Char_t *);
  void DBComment(Char_t *x) {res->SetComment(x);}
  void DBPrint() {res->Print();}
  void DBSet(Int_t i,Float_t x) {res->ep[i]=x;}
 Int_t DBSend() {res->Print(); res->CreateEntry(false,false); return(res->WriteEntry());}
 //  void DBSetTau(Float_t x) {DBSet(10,x);}
  //  void DBSetSign(Float_t x) {if(x<0) DBSet(3,-10); else DBSet(3,10);}
#endif
  
  //  MeasureWF(Char_t *,Int_t,Int_t,Int_t,Float_t,Float_t);
  MeasureWF(Int_t=60);
  MeasureWF(Char_t *,Int_t=5001,Float_t=0);
  MeasureWF(Int_t,Char_t *,Float_t=0);
  virtual ~MeasureWF();

  void AddHisto(Int_t,Float_t,TH1F *); //voltage index, voltage, histogram
  void AddHisto(Float_t,TH1F *); //voltage, histogram
  //Voltage Current Section
  inline void SetAnnealTime(Float_t x) {AnnealTime=x;}
  inline Float_t GetAnnealTime() {return AnnealTime;}
  void SetVoltages(Float_t *voltages) {Voltages.Adopt(Multiple,voltages);};
  void SetVoltage(Int_t index, Float_t voltage) {Voltages[index]=voltage;};
  void SetHistoTime(int ind,Float_t start);
  void SetHistoTime(int num,Float_t *start);
  Float_t GetVoltage(Int_t index) {return(Voltages[index]);}
  Int_t GetIndex(Float_t Volt) {for(Int_t i=0;i<Multiple;i++) if(Voltages[i]==Volt) return(i); printf("No such measurement!\n"); return(-1);}
  Float_t GetCurrent(Int_t index) {return(Current[index]);}
  Float_t GetCurrent(Float_t Volt) {for(Int_t i=0;i<Multiple;i++) if(Voltages[i]==Volt) return(Current[i]); printf("No such measurement!\n"); return -1;}
  void SetVoltages(Float_t svol,Float_t step) {for(Int_t z=0;z<Multiple;z++) Voltages[z]=svol+step*(Float_t)z;};
  void SetVoltages(Int_t num,Float_t svol,Float_t step) {for(Int_t z=num;z<Multiple;z++) Voltages[z]=svol+step*(Float_t)(z-num);};
  void SetTemperatures(Float_t T)  {for(Int_t z=0;z<Multiple;z++) Temperature[z]=T;};
  void SetCapacitance(Double_t x) {CAP=x;}
  void PrintVoltages() {printf("Defined Voltages::\n"); for(Int_t i=0;i<Multiple;i++) printf("(%d,%f)\n",i,Voltages[i]);}
  //Input Output section
  //Draw Section
  void EffTau(Int_t, Int_t,Float_t,Float_t,Int_t,Float_t,Float_t=0.03); //Determination of effective doping concentration from the slope
  void EffTauSave(Int_t, Int_t,Float_t*,Float_t*,Int_t,Float_t *,Int_t,Float_t=0.03);
  void EffTau(Int_t, Int_t,Float_t*,Float_t*,Int_t,Float_t,Float_t=0.03);
  void EffTauComplex(Int_t, Int_t,Float_t,Float_t,Int_t,Float_t,Float_t=0.03);
  void EffTauComplex(Int_t, Int_t,Float_t*,Float_t*,Int_t,Float_t,Float_t=0.03); 
  //Trapping time section 
  TGraph *Tau(Float_t ,Int_t , Int_t ,Float_t *, Float_t *,Int_t=0,Int_t=0, TArrayI=0);
  TGraph *Tau(Float_t ,Int_t , Int_t ,Float_t=0, Float_t=50,Int_t=0,Int_t=0, TArrayI=0);
  TGraph *TauVolt(Float_t ,Float_t ,Int_t , Int_t ,Float_t=0, Float_t=50,Int_t=1,Int_t=0);
  TGraph *VoltTau(Float_t ,Float_t ,Int_t , Int_t ,Float_t=0, Float_t=50,Int_t=0,Int_t=0,Int_t=1, TArrayI=0);
  TGraph *Tau(Elec *,Float_t ,Int_t , Int_t ,Float_t=0, Float_t=50,Int_t=0,Int_t=0, TArrayI=0);

 //Electric Field Section
  TH1F *ElField(Int_t , Float_t , Float_t, Float_t, Int_t=-1,Int_t=2,Int_t=0);

  //Draw Section

  void Draw(Float_t Volt,Option_t *opt,Float_t low=-1111,Float_t high=-1111) {if(GetIndex(Volt)==-1) return; else Draw(GetIndex(Volt),opt,low,high);}; // Voltage, Graphics Option, low bin , high bin
  void DrawTest(int);

  void Draw(Int_t,Option_t *,Float_t=-1111,Float_t=-1111);   // Voltage Index, Graphics Option, low bin , high bin
  void DrawMulti(Float_t=-1111.,Float_t=-1111.,Int_t=-1,Int_t=-1,Int_t =1,Int_t=0,Int_t=-1111); // low bin, high bin, Start Voltage Index, End Voltage Index, Int_t step, Int_t model
  void Legend(TH1 *,Int_t,Int_t,Int_t=1,Int_t =0);
  TGraph *DrawIV(Int_t=0); // current vs. voltage 
  TGraph *DrawTV(Int_t=0); // tempetature vs. voltage

  // rise time vs position 
  TGraph *RiseTime(Float_t=-1111.,Float_t=-1111.,Int_t=-1,Int_t=-1,Int_t =1,Int_t=0,Int_t=-1111); // tempetature vs. voltage
  TGraph *AnalysePSTCT(Int_t=-1,Int_t=-1,Int_t =1,Float_t=0, Float_t=0,Int_t=2,Int_t=1,Int_t=1); // tempetature vs. voltage
  TGraph *PosTime(Float_t=-1111.,Float_t=-1111.,Int_t=-1,Int_t=-1,Int_t =1,Int_t=0,Int_t=-1111); // tempetature vs. voltage


  //
  //Histogram Section 
  void CompareHisto(TH1F *,Float_t); // Plots Two Histograms on top of each other :: histogram , voltage
  void GetHistogram(Int_t number,TH1F *his) {((TH1F *)histo->At(number))->Copy(*his);}; //voltage index, histogram to copy original to
  void GetHistogram(Float_t voltage,TH1F *his) {for(Int_t i=0;i<Multiple;i++) if(Voltages[i]==voltage) ((TH1F *)histo->At(i))->Copy(*his);}; //voltage, histogram
  TH1F *GetHA(Double_t voltage)  {Int_t index=0; for(Int_t i=0;i<Multiple;i++) if(Voltages[i]==voltage) index=i; return((TH1F *)histo->At(index));};
  TH1F *GetHA(Int_t index)  {return((TH1F *)histo->At(index));};
  inline TH1F *operator()(Double_t voltage) {return(GetHA(voltage));};
  inline TH1F *operator()(Int_t index) {return((TH1F *)histo->At(index));};
  //TH1F *GetHA(Int_t index) {return((TH1F *)histo->At(index));};
  void ScaleHisto(Float_t fac=-1) { for(int i=0;i<Multiple;i++)  ScaleHisto(i,fac); };
  void ScaleHisto(Int_t number, Float_t fac) {((TH1F *)histo->At(number))->Scale(fac);}; //voltage index , factor to scale
  //Integral Section
  void GetIntegral(Float_t*,Float_t=1,Float_t=-1111,Float_t =-1111); //  in - array of integrals, scale , min time , high time 
  void GetIntegral(Float_t*,Float_t*,Float_t=1,Float_t=-1111,Float_t =-1111); // in - array of voltages, in - array of integrals, scale , min time , high time 
  Float_t Integral(Float_t,Float_t=-1111,Float_t =-1111); // voltage, mint, maxt of the integration 
  Float_t Integral(Int_t,Float_t=-1111,Float_t =-1111); // index of the voltage, mint, maxt of the integration
  TGraph *CCE(Float_t=-1111,Float_t=-1111,Int_t=0,Int_t=1); //Draw CCE: start time, end time, model(sqrt,lin), option (time , T)
  TGraph *CCEFit(Float_t=-1111,Float_t=-1111,Float_t=1, Float_t=200, Int_t=0,Int_t=1); //Draw CCE: start time, end time, model(sqrt,lin), option (time , T)
  TGraph *CCEE(Elec *,Float_t=-1111,Int_t=0,Int_t=1);       //Draw CCEE: electrinics, normalization, model(sqrt,lin), option (time , T)
  Float_t GetFDV(Float_t, Float_t,Float_t,Float_t,Float_t, Float_t,Int_t=0,Int_t=0); //Get FDV: start time, end time, intervals (4x), model, show
  Float_t GetFDV(Elec *,Float_t,Float_t,Float_t, Float_t,Int_t=0,Int_t=0); //Get FDV: electronics, intervals (4x), model, show
  TH1F *Derivation(Float_t,Int_t =6,Int_t=1);
  void  Derivation(Int_t, Int_t,Int_t=1);
  TH1F *ChargeShape(Float_t);
  void  ChargeShape(Int_t, Int_t,Int_t=1);
  TH1F *EShaping(Elec *,Int_t);
  TH1F *EShaping(Elec *el,Float_t Volt) {if(GetIndex(Volt)!=-1) return(EShaping(el,GetIndex(Volt))); else return(new TH1F()); }

  void CorrectBaseLine(Int_t=0);
  // General section
  Int_t GetEntries() {return(Multiple);};
  // Peak section 
  TH1F *GetVelHisto(Float_t,Float_t, Int_t =0 );
  double  GetVelField(TH1F *, Float_t, Float_t, Float_t);
  double  GetElField(TH1F *,Float_t , Float_t ,Float_t );
   double GetEVel(double,double=-1);
   double GetVelE(double,double=-1);
  ///////////////////////////////////////////////////////////////
  Int_t int2ascii(Char_t v[],Float_t,Int_t=1);
  Double_t Mobility(Float_t,Float_t ,Float_t ,Double_t, Int_t=0);
  void NormArray(Int_t,Float_t *);
  void Info(); 
  Float_t GetTime(Float_t=3600);
  Float_t GetT() {Float_t sumT=0; for(Int_t i=0;i<Multiple;i++) sumT+=Temperature[i]; return(sumT/Multiple); };
  Float_t GetT(Float_t Volt) {return (Temperature[GetIndex(Volt)]);};
  Float_t GetT(Int_t index) {return (Temperature[index]);};
  //  void realft(float *, unsigned long , int);
  //  void four1(float *, unsigned long , int);
  
  ClassDef(MeasureWF,2) 
};


#endif
















