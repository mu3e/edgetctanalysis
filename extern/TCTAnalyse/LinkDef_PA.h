#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class PSTCT+;
#pragma link C++ class TCTScan+;
#pragma link C++ class MeasureWF+;
#pragma link C++ class Elec+;
#pragma link C++ class TCTReflect+;

#pragma link C++ global gROOT;

#endif









