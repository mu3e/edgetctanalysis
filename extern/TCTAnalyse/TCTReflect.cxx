// This class handels the voltage scans produced with TCT Labview software (".tct" files)!
// Waveform vectors are stored in the histograms on which the data manipultaion rutines work

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "TH1F.h"
#include "TMath.h"
#include "TCTReflect.h"

ClassImp(TCTReflect)

TCTReflect::TCTReflect(Float_t x,Float_t y,Float_t z)
{
  RefC=x;
  RefTs=y;
  RefTe=z;
  SubHis=NULL;
  SubHisCopy=0;
}

TH1F *TCTReflect::CorrReflCopy(TH1F *his)
{

 TH1F *hiscopy=new TH1F();
 his->Copy(*hiscopy);
 CorrRefl(hiscopy);
 
 //hiscopy->SetLineColor(2);
 return hiscopy;
}

void TCTReflect::CorrRefl(TH1F *his)
{
 Int_t bin,k;
 Int_t lb=his->GetXaxis()->FindBin(RefTs);
 Int_t hb=his->GetXaxis()->FindBin(RefTe);
 for(Int_t i=0;i<his->GetNbinsX();i++)
    {
      bin=his->GetBinContent(i);
      k=(i-lb)/(hb-lb);
      if(k>0)  his->SetBinContent(i,bin-TMath::Power(RefC,k)*his->GetBinContent(i-k*(hb-lb)));
    }

}


TH1F *TCTReflect::SubRefWF(TH1F *his,Int_t copy)
{
  //  Int_t ret;
  //  Int_t numhis,numsub,num;
  TH1F *hiscopy, *hisw;
  //  Char_t txtname[200],txttitle[200];
  //  num=his->GetNbinsX(); 
  //  numsub=hissub->GetNbinsX(); 
  
  if(copy) 
    {
     hiscopy=new TH1F();
     his->Copy(*hiscopy);
     hisw=hiscopy;  
//       sprintf(txtname,"Copy of %s",his->GetName());
//       sprintf(txttitle,"Copy of %s",his->GetTitle());
//       hiscopy=new TH1F(txtname,txttitle,num,his->GetBinLowEdge(0),his->GetBinUpEdge(num));      
//       hisw=hiscopy;
    }
  else hisw=his;
  
  hisw->Add(SubHis,-1);
  
  //  num=numhis>=numsub?numsub:numhis;
  
  return hisw;
}


void TCTReflect::SetSubRefWF(TH1F *his)
{
  SubHis=new TH1F();
  his->Copy(*SubHis);

}
