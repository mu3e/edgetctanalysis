#ifndef _TCTScan 
#define _TCTScan

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TCT Tamperature Scan                                                 //
//                                                                      //
//                                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "stdio.h"
#include "stdlib.h"
#include "MeasureWF.h"
#include "TCTReflect.h"
#include "TH2F.h"
#include "TGraphErrors.h"


class  TCTScan : public TObject {

private:
Int_t Num;
TClonesArray *vf;          //->

Int_t i2a(Char_t *,Float_t,Int_t=1);
 public:
 TPaveText *pt;            //!

  TCTScan(Int_t=40);
  ~TCTScan();
  Int_t GetNum() {return(Num);};
  void  SetNum(Int_t);
  void AddMeasureWF(Int_t,Char_t *,Float_t,Int_t=1000);
  void AddMeasureWF(Int_t,MeasureWF*);
  void Merge(TCTScan *);
  void ReadFile(Char_t *,Int_t *step,Int_t count,Float_t shift, Int_t=1000);
  //  TH1F *GetHisto(Int_t i,Float_t volt) { return(Get(i)->) };
  void NormArray(Int_t,Float_t *);
  void Info(Int_t=-1111, Int_t=-1111);
  TGraph *DrawIT(Float_t,Int_t=0);
  void DrawMulti(Float_t,Float_t=-1111.,Float_t=-1111.,Int_t=-1,Int_t=-1,Int_t =1,Int_t=1);
  void DrawMultiCCE(Float_t,Float_t, Int_t=-1 , Int_t=-1, Int_t=0 );
  void DrawMultiCS(Float_t,Float_t=-1111.,Float_t=-1111.,Int_t=-1,Int_t=-1);
  TGraph *CCE(Float_t,Int_t=0,Float_t=-1111,Float_t =-1111);
  TGraph *CCEE(Elec *,Float_t,Int_t=0);
  TGraphErrors *FDV(Float_t, Float_t,Float_t,Float_t,Float_t, Float_t,Int_t =0 ,Int_t=0,Float_t=-1111,Float_t=1,Float_t=0,Float_t=0);
  TGraph *FDV(Elec *,Float_t,Float_t,Float_t, Float_t,Int_t=0,Int_t =0 );
  void CorrectBaseLine(Int_t method=0) {for(Int_t i=0;i<Num;i++) Get(i)->CorrectBaseLine(method);};
  void Legend(TH1F *, Int_t, Int_t,Int_t=1);
#ifdef RHDB  
  void DBResult(Char_t *user) {for(Int_t i=0;i<Num;i++) Get(i)->DBResult(user);}
#endif
  void SetCapacitance(Double_t x) {for(Int_t i=0;i<Num;i++) Get(i)->CAP=x;};
  inline TH1F *GetHA(Int_t index, Float_t Volt) {return(Get(index)->GetHA(Volt));};
  inline TH1F *operator()(Int_t index, Float_t Volt) {return(GetHA(index,Volt));};
  inline MeasureWF *Get(Int_t index) {return((MeasureWF *)vf->At(index));}
  inline MeasureWF *operator()(Int_t index) {return((MeasureWF *)vf->At(index));};
  
  ClassDef(TCTScan,2) 
};
#endif




#ifndef _PSTCT
#define _PSTCT

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TCT Position Sensitive                                               //
//                                                                      //
// fortmat:                                                             //
// Line 0: Writing type of measurement. 11 = waveform,                  //
// 12 = waveform maximum only, 13 = waveform maximum and integrals      //
// Line 1: Datum                                                        //
// Line 2: Absolute time in s at start                                  //
// Line 2: x0, dx, Nx                                                   //
// Line 3: y0, dy, Ny                                                   //
// Line 4: z0, dz, Nz                                                   //
// Line 5: Nvoltages, Voltages                                          //
// Line 6: t0, dt, Npoints                                              //
// Line a->oo: x y z U tmeas                                            //
// Line b->oo: Npoints meritve                                          //
// Line c->oo: Npoints meritve                                          //
// Line d->oo: Npoints meritve                                          //
// The class TCT scan is used in x                                      //
// The class TCT scan is used in y                                      //
//////////////////////////////////////////////////////////////////////////

class  PSTCT : public TCTReflect {

private:
   void  swoo(char *, char *);
   void  swooip(float *, int);
   int BLE_CODE;
 public:

 FILE *in;
 TArrayI 	 Date;		//Date of the Measurement (or simulation)
 TClonesArray   *histo1;        //-> 
 TClonesArray   *histo2;        //-> 
 TClonesArray   *histo3;        //-> 
 TClonesArray   *histo4;        //->
 Float_t *xyz[9];             // x coordinates coresponing to histo array change from 8
 TArrayF ta;                  // ta coordinates coresponing to histo array
 Int_t abstime;
 Int_t Nx,Ny,Nz;
 Int_t RefInd;
 Float_t dx,dy,dz;
 Float_t x0,y0,z0;
 Int_t NU1,NU2;
 TArrayF U1,U2,I1,I2;                    // Array of voltages
 Float_t t0,dt;
 Int_t NP;
 Int_t WFOnOff[4];
 Int_t type;
 Int_t numxyz;
 //Header
 Float_t T;                    // temperature
 Float_t Source;               // type of e-h generation
 Char_t *User;                 // user taking the measurements
 Char_t *Sample;               // Sample name
 Char_t *Comment;              // Comment

 PSTCT(Char_t *, Float_t=0,Int_t=0);   
  ~PSTCT(){};
  void  ReadWFs(Float_t=0);
  void  ReadWFsBin(Float_t=0);
  Int_t indx(Int_t , Int_t , Int_t, Int_t , Int_t );
  Float_t GetWidth(TH1F *, Int_t &, Int_t &,  Float_t=0, Float_t=25, Float_t=-1111, Float_t=-1111, Int_t = 50);
  TH1F *GetHA(Int_t , Int_t);
  TH1F *GetHA(Int_t , Int_t, Int_t, Int_t, Int_t=0, Int_t=0);
  TGraph *GetIV() {TGraph *gr=new TGraph(U1.GetSize(),U1.GetArray(),I1.GetArray()); return gr;}
  TH1F *ModGetHA(Int_t, Int_t, Float_t=-1111, Float_t=-1111);
  TH1F *ModGetHA(Int_t, Int_t, Int_t, Int_t, Int_t=0, Int_t=0, Float_t=-1111, Float_t=-1111);
  TH1F *AverageGetHA(Int_t, Int_t=-1111, Int_t=-1111, Int_t=0, Int_t=0, Int_t=0, Int_t=0, Int_t=0);  

  TH2F *Draw(Int_t=0, Int_t=0, Int_t=0, Int_t=0, Int_t=0, Int_t=0, Float_t=0, Float_t=25);
  //TH2F *Draw(Int_t=0, Int_t=0, Int_t=0, Int_t=0, Float_t=0, Float_t=25);
  //TGraph *DrawIV(Int_t t);
  TGraph *DrawArray(Int_t, Int_t);
  void DrawList(Int_t, Int_t *);
  void DrawList(Int_t, Int_t*, Int_t *);
  void PrintInfo();
  void CorrectBaseLine(Float_t=0.0);
  void cords(Int_t , Int_t , Int_t , Int_t, Int_t);
  MeasureWF *Projection(int ch, int dir,int x,int y,int z, int nu1, int nu2, int num);
  MeasureWF *Projection(int , int *);
  //  TH2F *Draw(Int_t ch, int plane,int x,int y,int z, int nu1, int nu2, int num););
ClassDef(PSTCT,1) 
};
#endif















