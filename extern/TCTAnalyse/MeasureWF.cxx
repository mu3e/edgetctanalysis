// This class handels the voltage scans produced with TCT Labview software (".tct" files)!
// Waveform vectors are stored in the histograms on which the data manipultaion rutines work
#include "Rtypes.h"
#include <math.h>
//#include <Riostream>
#include <stdio.h>
#include <stdlib.h>
#include "MeasureWF.h"
#include <string.h>
#include "TF1.h"
#include "TDatime.h"
#include "TCanvas.h"

ClassImp(MeasureWF)


//____________________
// MeassureWF
//                                                             //
// This class handels the voltage scans produced with TCT Labview software (".tct" files)!
// Waveform vectors are stored in the histograms on which the data manipultaion rutines work

//MeasureWF::MeasureWF(Char_t *name,Int_t num, Int_t start,Int_t bins,Float_t startbin,Float_t endbin)
  MeasureWF::MeasureWF(Int_t num)
{
  // Default Constructor
  // 		Int_t num    ; Number of aqusitions
  Multiple=num;
  histo =new TClonesArray("TH1F",Multiple);
  histo->BypassStreamer(kFALSE);
  Voltages=TArrayF(Multiple);
  Temperature=TArrayF(Multiple);
  Current=TArrayF(Multiple);
  Frequencies=TArrayF(Multiple);
  Date=TArrayF(6);
  Frequency=0; 

  Date.Reset();
  Frequencies.Reset();
  Voltages.Reset();
  Date.Reset();
  Current.Reset();
  Temperature.Reset();
  CAP=13e-12;
  DrawMode=true;
  strcpy(suffix," V");
  strcpy(prefix,"U=");
#ifdef RHDB
  res=NULL;
#endif
  pt=NULL;
}


//CERN DATA LOADER //

MeasureWF::MeasureWF(Int_t NumOfFiles,Char_t *FileName,Float_t offset)
{
  // Int_t  NumOfFiles; number of files to be read
  // Char_t *FileName; file name without serial number and extension. The files are supposed to end with .txt
  // Float_t offset; offset in time. In order to use CorrectBaseLine function it is recomended that signal starts at t=0 ns

  FILE *in;
  Int_t Select[]={2,2,3,1,1,3,2,2,1,2,3,1,1,1,1,1,1,1,1,1,1,1};
  Int_t Relevant[]={1,-1,-1,-1,-1,-1,-1,-1,-1,-1,2,3,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  Char_t Comment[100];
  Char_t FileNameAll[200];
  Int_t NumPoint,header=1;
  Float_t SamPer,data,Freq,Curr,Temp,U,factor=1,timedata;
  Multiple=NumOfFiles;
  Int_t q,i,j;

  Char_t  name[100],temp1[100],temp2[100],temp3[100];

  // ALLOCATE SPACE
  histo =new TClonesArray("TH1F",Multiple);
  histo->BypassStreamer(kFALSE);
  Date=TArrayF(6);
  Voltages=TArrayF(Multiple);
  Temperature=TArrayF(Multiple);
  Current=TArrayF(Multiple);
  Frequencies=TArrayF(Multiple);
  TClonesArray &entryp = *histo;   
  strcpy(FileNameAll,FileName);

  for(Int_t k=0;k<Multiple;k++)
    { 
      sprintf(FileNameAll,"%s%d.txt",FileName,k+1);
      printf("Reading File Name=%s ::: ",FileNameAll);
      if( (in=fopen(FileNameAll,"r+"))==NULL ) {printf("Can not read the file\n"); exit(0); }

      printf("\nChecking the format\n");
      header=1;
      while(header)
	{
	  fscanf(in,"%s",temp1);
	  //  printf("%s\n",temp1);
	  if(strchr(temp1,':')==&temp1[0])
	    {
	      fscanf(in,"%s",temp2);
	      if(strchr(temp2,':')!=&temp2[0])
		{
		   
		  if(!strcmp(temp1,":start"))
		    {
		      for(q=0;q<10;q++) if(temp2[q]=='/') temp2[q]='\0'; 
		      Date[0]=atoi(temp2); Date[1]=atoi(&temp2[3]); Date[2]=atoi(&temp2[6]); 
		    }

		  if(!strcmp(temp1,":Bias") && !strcmp(temp2,"Volatge:"))
		    {
		      fscanf(in,"%s",temp3);
		      Voltages[k]=atof(temp3); 
		      printf("Napetost = %f\n",Voltages[k]);
		    }
		  if(!strcmp(temp1,":Points"))
		    {
		      NumPoint=(Int_t)atof(temp2); 
		      printf("Points = %d\n",NumPoint);
		    }
		  if(!strcmp(temp1,":Detla") && !strcmp(temp2,"t[ns]"))
		    {
		      fscanf(in,"%s",temp3);
		      SamPer=atof(temp3); 
		      if(SamPer<1e-6) SamPer*=1e9; 
		      printf("Delta t[ns] = %f\n",SamPer);
		    }

		  if(!strcmp(temp1,":desired") && !strcmp(temp2,"temperature[C]"))
		    {
		      fscanf(in,"%s",temp3);
		      Temperature[k]=atof(temp3); 
		      printf("T = %f",Temperature[k]);
		    }
		  //printf("%s\n",temp2);
		  if(!strcmp(temp2,"BEGIN")) {header=0; break;}
		}
	    
	      
	    }
	  if(!strcmp(temp1,"BEGIN")) {header=0; break;}
    
	}


      Frequency=-1;

      //fscanf(in,"%s",Comment);
      //fscanf(in,"%s",Comment);
      //printf("%d\n",NumPoint);

      sprintf(name,"Measured WF %5.2f V",Voltages[k]);
      //printf("Histo Name = %s\n",buff);
      new(entryp[k]) TH1F(name,name,NumPoint,offset,NumPoint*SamPer+offset);



      // printf("\n%s\n",(const Char_t *)(histoid+" "+vol));
      //plot=new TH1F("his","h1",NumPoint,offset,NumPoint*SamPer-offset);

      for(i=0;i<NumPoint;i++)
	{
	  fscanf(in,"%e %e",&timedata,&data); //modified 11.2.2011 to match new format
	  ((TH1F*)entryp[k])->SetBinContent(i,data*factor);
	} 

      // if(k==0) plot.Draw(); else plot.DrawCopy("SAME");
      // delete plot;
      fclose(in);
      printf(" O.K.\n",FileNameAll);
    }

  CAP=13e-12;
  DrawMode=true;
  strcpy(suffix," V");
  strcpy(prefix,"U=");


  pt=NULL;
#ifdef RHDB
  res=NULL;
#endif
}


///////////////////////
// OLD UNI-HH DATA LOADER ///
///////////////////////

// MeasureWF::MeasureWF(Int_t NumOfFiles,Char_t *FileName,Float_t offset)
// {
// FILE *in;
// Int_t Select[]={2,2,2,2,3,3,3,3,2,4,3,5,3,3,5,5,4,4,4,4,3,2};
// Int_t Relevant[]={1,1,1,-1,-1,2,-1,2,-1,-1,2,-1,2,-1,-1,-1,-1,-1,-1,3,2,1};
// Char_t Comment[100];
// Char_t FileNameAll[200];
// Int_t NumPoint;
// Float_t SamPer,data,Freq,Curr,Temp,U,factor=1;
// Multiple=NumOfFiles;
// Int_t q,i,j;

// Char_t  name[100];

// // ALLOCATE SPACE
// histo =new TClonesArray("TH1F",Multiple);
// histo->BypassStreamer(kFALSE);
// Date=TArrayF(6);
// Voltages=TArrayF(Multiple);
// Temperature=TArrayF(Multiple);
// Current=TArrayF(Multiple);
// Frequencies=TArrayF(Multiple);
// TClonesArray &entryp = *histo;   
// strcpy(FileNameAll,FileName);

// for(Int_t k=0;k<Multiple;k++)
// { 
//   //sprintf(FileNameAll,"%s%c",FileName,k+97);
//   FileNameAll[strlen(FileName)-1]=FileName[strlen(FileName)-1]+k;
// printf("Reading File Name=%s ::: ",FileNameAll);
// if( (in=fopen(FileNameAll,"r+"))==NULL ) {printf("Can not read the file\n"); exit(0); }

// //printf("Checking the format\n");

// fscanf(in,"%s",Comment);
// //printf("%s\n",Comment);
// for(i=0;i<22;i++)
//   {
//     for(j=0;j<Select[i];j++)
//       {
// fscanf(in,"%s",Comment);
// //printf("%s\n",Comment); 
//  if(Relevant[i]==j) 
//    {
//      switch(i)
//        {
//        case 1:  for(q=0;q<10;q++) if(Comment[q]=='-') Comment[q]='\0'; 
// 	        Date[0]=atoi(Comment); Date[1]=atoi(&Comment[3]); Date[2]=atoi(&Comment[6]); break;
//        case 2: for(q=0;q<10;q++) if(Comment[q]=='-') Comment[q]='\0'; 
// 	        Date[3]=atoi(Comment); Date[4]=atoi(&Comment[3]); Date[5]=atoi(&Comment[6]); break;
//        case 5:   Temperature[k]=atof(Comment)-273;  /* printf("Temp %f ",Temp);*/ break;
//        case 7:   Current[k]=atof(Comment)*1e-6;   /*printf("CURR %f ",Curr);*/ break;
//        case 10:  Voltages[k]=-atof(Comment);  /* printf("U %f ",Voltages[k]);*/ break;
//        case 12:  Frequency=atof(Comment);     /* printf("FREQ %f ",Frequency);*/  break;
//        case 19:  SamPer=atof(Comment);        /* printf("SAMPER %f ",SamPer);*/ break; 
//        case 20:  NumPoint=atoi(Comment);      /* printf("ATOI %d ",NumPoint);*/ break; 
//        case 21:  if(!strcmp(Comment,"[mV]")) factor=1e-3; 
// 	         if(!strcmp(Comment,"[V]")) factor=1; // printf("Comment=%s %f\n",Comment,factor);
// 		 break;
//        } 
//    }  
//       }
//     // printf(":::::: %d %d\n",i,Relevant[i]);
//   }
// fscanf(in,"%s",Comment);
// //fscanf(in,"%s",Comment);
// //printf("%d\n",NumPoint);

// sprintf(name,"Measured WF %5.2f V",Voltages[k]);
// //printf("Histo Name = %s\n",buff);
// new(entryp[k]) TH1F(name,name,NumPoint,offset,NumPoint*SamPer-offset);



// // printf("\n%s\n",(const Char_t *)(histoid+" "+vol));
// //plot=new TH1F("his","h1",NumPoint,offset,NumPoint*SamPer-offset);

// for(i=0;i<NumPoint;i++)
// {
//   fscanf(in,"%e",&data);
//   ((TH1F*)entryp[k])->SetBinContent(i,data*factor);
// } 

// // if(k==0) plot.Draw(); else plot.DrawCopy("SAME");
// // delete plot;
// fclose(in);
// printf(" O.K.\n",FileNameAll);
// }
//  DrawMode=true;
//  strcpy(suffix," V");
//  strcpy(prefix,"U=");
// CAP=13e-12;
// pt=NULL;
// #ifdef RHDB
// res=NULL;
// #endif
// }

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
MeasureWF::MeasureWF(Char_t *FileName,Int_t Bins,Float_t Start)
{
  // Constructor
  // 		Char_t *FileName ; File Name
  // 		Int_t bins       ; Number of bins per waveform
  // 		Int_t Start       ; Shift in time (Start time)
  FILE *in;
  Float_t t0,deltat,data,time,Power;
  Int_t show=1,Points;

  CAP=13e-12;
  DrawMode=true;
  strcpy(suffix," V");
  strcpy(prefix,"U=");

  Date=TArrayF(6);

  if((in=fopen((const Char_t *)FileName,"r+"))==NULL) {printf("\n Error opening file for reading\n"); return;}

  fscanf(in,"%f %f %f %f %f %f\n",&Date[0],&Date[1],&Date[2],&Date[3],&Date[4],&Date[5]);
  if(show) printf("DATE= %4.0f %4.0f %4.0f %4.0f %4.0f %4.0f\n",Date[0],Date[1],Date[2],Date[3],Date[4],Date[5]);
  fscanf(in,"%f %f\n",&Frequency,&Power); 
  if(show) printf("Frequnecy=%5.2f GeneralVar=%5.2f\n",Frequency,Power); 
  fscanf(in,"%e %e %d %d\n",&t0,&deltat,&Points,&Multiple);  
  // if(deltat>1e-6) { deltat*=1e-9; t0*=1e-9;}
  if(show) printf("Start Time=%5.2e, TimeStep=%5.2e, Points=%d, NumVoltages=%d\n",t0,deltat,Points,Multiple); 

  histo =new TClonesArray("TH1F",Multiple);
  histo->BypassStreamer(kFALSE);
  Voltages=TArrayF(Multiple);
  Temperature=TArrayF(Multiple);
  Current=TArrayF(Multiple);
  //Amplitudes=TArrayF(Multiple*3*MaxPeaks+1);
  fclose(in);
  pt=NULL;
  ReadTCTForm(FileName,Bins,Start);
}

void MeasureWF::ReadTCTForm(TString FileName, Int_t Bins, Float_t StartBin, Float_t EndBin, Int_t show)
{
  // Reads TCT Form File (internaly used by constructor)! 
  // 	      TString FileName ;  file name
  // 	      Int_t Bins       ;  number of bins
  // 	      Float_t StartBin ;  StartBin of the histogram (values are multiplied with 1e9 - scale in ns
  // 	      Float_t EndBin   ;  EndBin of the histogram
  // 	      Int_t Show       ;  verbose reading
  FILE *in;
  Int_t npoints,X0;
  Int_t i,j,k;
  Float_t PulseG[2];
  Float_t Graph[3];
  Float_t t0,deltat,data,time,Power;
  Int_t Points;
  //Float_t *Voltages=new Float_t[Multiple];
  TClonesArray &entryp = *histo;   
  Char_t  vol[4]="xxx";
  TString histoname="Measured Waveform";
  TString histoid="MWF";

  if((in=fopen((const Char_t *)FileName,"r+"))==NULL) {printf("\n Error opening file for reading\n"); return;}
  fscanf(in,"%f %f %f %f %f %f\n",&Date[0],&Date[1],&Date[2],&Date[3],&Date[4],&Date[5]);
  if(show) printf("DATE= %4.0f %4.0f %4.0f %4.0f %4.0f %4.0f\n",Date[0],Date[1],Date[2],Date[3],Date[4],Date[5]);
  fscanf(in,"%f %f\n",&Frequency,&Power); 
  if(show) printf("Frequnecy=%5.2f GeneralVar=%5.2f\n",Frequency,Power); 
  fscanf(in,"%e %e %d %d\n",&t0,&deltat,&Points,&Multiple);  
  if(show) printf("Start Time=%5.2e, TimeStep=%5.2e, Points=%d, NumVoltages=%d\n",t0,deltat,Points,Multiple); 

  for(i=0;i<Multiple-1;i++) {fscanf(in,"%f ",&Temperature[i]); if(show) printf("%5.1f ",Temperature[i]); }; 
  fscanf(in,"%f\n",&Temperature[i]); if(show) printf("%5.1f\n",Temperature[i]);
  for(i=0;i<Multiple-1;i++) {fscanf(in,"%f ",&Voltages[i]); if(show) printf("%5.2f ",Voltages[i]);}; 
  fscanf(in,"%f\n",&Voltages[i]); if(show) printf("%5.2f \n",Voltages[i]);
  for(i=0;i<Multiple-1;i++) {fscanf(in,"%e ",&Current[i]); if(show) printf("%5.2e ",Current[i]); }; 
  fscanf(in,"%e\n",&Current[i]); if(show) printf("%5.2e\n",Current[i]);

  if(deltat<1e-6) {deltat*=1e9; t0*=1e9;}
  if(StartBin==-1111) StartBin=t0;; 
  if(EndBin==-1111) EndBin=StartBin+deltat*Points; 
  if(show) printf("Start Bin=%5.2e, End Bin=%5.2e, Bins=%d\n",StartBin,EndBin,Bins);

  //added by matej to get rid of annoying warnings about potential memery leak 
  for(i=0;i<6;i++){
    TString c="";
    c+=Date[i];
    histoid+="_";
    histoid+=c.Strip(TString::kLeading,' ');
  }

  for(i=0;i<Multiple;i++) 
    {
      vol[0]=(Char_t)(((Int_t) fabs(Voltages[i]))/100)+48;
      vol[1]=(Char_t)(((Int_t) fabs(Voltages[i])%100)/10)+48;
      vol[2]=(Char_t)((Int_t) fabs(Voltages[i])%10)+48; //printf("vol=%s\n",vol); 
      new(entryp[i]) TH1F((const Char_t *)(histoid+vol),(const Char_t *)(histoname+"_"+vol),Bins,StartBin,EndBin);
    }

  for(j=0;j<Points;j++) 
    for(i=0;i<Multiple;i++)
      {
	if(i==Multiple-1) {fscanf(in,"%e\n",&data);} else {fscanf(in,"%e\t",&data);}
	time=(Float_t)j*deltat+StartBin;
	((TH1F*)entryp[i])->Fill(time,(Double_t)data);
	// if(i==Multiple-1) {printf("%e\n #=%d::::::",data,j);} else {printf("%e\t",data);}
      }  

  while(!feof(in))
    {
      printf("%c",getc(in));
    }

  fclose(in);
  if(show) printf("File:: %s read! \n",(const Char_t *)FileName); 
}



//////////////////////////////////////////////////////////////////////
////////UNI HH DATA FORMAT - COMMENTED OUT IN LJUBLJANA CODE /////////
//////////////////////////////////////////////////////////////////////
// MeasureWF::MeasureWF(Char_t *FileName,Int_t Bins,Float_t Start)
// {
// // Constructor
// // 		Char_t *FileName ; File Name
// // 		Int_t bins       ; Number of bins per waveform
// // 		Int_t Start       ; Shift in time (Start time)
// FILE *in;
// Float_t t0,deltat,data,time,StartBin,EndBin;
// Int_t show=1,Points,ok=1,RampMode,i,j;
// Char_t buff[1000],temp[100];
// Date=TArrayF(6);

// if((in=fopen((const Char_t *)FileName,"r+"))==NULL) {printf("\n Error opening file for reading\n"); return;}

//  Header = TString();  
//  fscanf(in,"%s",buff); Header.Append(buff); Header.Append("\n"); 
//  fscanf(in,"%s %f %f %f %f %f %f\n",temp,&Date[0],&Date[1],&Date[2],&Date[3],&Date[4],&Date[5]);
//  sprintf(buff,"%s %2.0f.%2.0f.%2.0f %2.0f:%2.0f:%2.0f\n",temp,Date[0],Date[1],Date[2],Date[3],Date[4],Date[5]);
//  Header.Append(buff);

//  for(i=0;i<5;i++)
//    {
//    fscanf(in,"%s",buff); Header.Append(buff); Header.Append(" ");  
//    fscanf(in,"%s",buff); Header.Append(buff); Header.Append("\n"); 
//    }

//  while(ok!=0)
//    {
//    fscanf(in,"%s",buff); 
//    ok=strcmp(buff,"Temperature_control_parameters_[stable,points,delay,heater_range]:");
//    if(ok==0) Header.Append("\n"); else Header.Append(" ");  Header.Append(buff);
//    }
//  ok=1; 
//  while(ok!=0)
//    {
//    fscanf(in,"%s",buff);
//    ok=strcmp(buff,"Osciloscope_control_parameters_[Ch,average,changeto,change@]:");
//    if(ok==0) Header.Append("\n"); else Header.Append(" "); Header.Append(buff);
//    }
//  ok=1; 
//  while(ok!=0)
//    {
//    fscanf(in,"%s",buff);
//    ok=strcmp(buff,"Laser_pulse_frequency[Hz]:");
//    if(ok==0) Header.Append("\n"); else Header.Append(" "); Header.Append(buff);
//    }
//  ok=1; 
//  while(ok!=0)
//    {
//    fscanf(in,"%s",buff);
//    ok=strcmp(buff,"VI_meter:");
//    if(ok==0) Header.Append("\n"); else Header.Append(" "); Header.Append(buff);
//    }
//  ok=1;
//  while(ok!=0) 
//    {
//    fscanf(in,"%s",buff);
//    ok=strcmp(buff,"VI_Control[delay,V_limint,average,compliance,U_after_scan]:");
//    if(ok==0) Header.Append("\n"); else Header.Append(" "); Header.Append(buff);
//    }
//  ok=1; printf("buff=%s\n",buff);
//  while(ok!=0)
//    {
//    fscanf(in,"%s",buff); 
//    ok=strcmp(buff,"Data_parameters[points,X0,dX,ramp_mode,steps,first_V,last_V,average_T]:");
//    if(ok==0) Header.Append("\n"); else Header.Append(" "); Header.Append(buff);
//    }
//  fscanf(in,"%d %e %e %d %d \n",&Points,&t0,&deltat,&RampMode,&Multiple);  
//  sprintf(buff,"%d %e %e %d %d ",Points,t0,deltat,RampMode,Multiple); Header.Append(buff);
//  ok=1; printf("buff=%s\n",buff);
//  while(ok!=0)
//    {
//    fscanf(in,"%s",buff);
//    ok=strcmp(buff,"BEGIN");  
//    if(ok!=0) { Header.Append(" "); Header.Append(buff); }
//    }

// histo =new TClonesArray("TH1F",Multiple);
// histo->BypassStreamer(kFALSE);
// TClonesArray &entryp = *histo;   
// Voltages=TArrayF(Multiple);
// Temperature=TArrayF(Multiple);
// Current=TArrayF(Multiple);
// Frequencies=TArrayF(Multiple);
//  DrawMode=true;
//  strcpy(suffix," V");
//  strcpy(prefix,"U=");
// CAP=13e-12;
// pt=NULL;
// #ifdef RHDB
// res=NULL;
// #endif


// if(show) printf("%s\n",(const Char_t *)Header);

//  for(i=0;i<Multiple-1;i++) {fscanf(in,"%f ",&Voltages[i]); if(show) printf("%5.2f ",Voltages[i]);}; 
//  fscanf(in,"%f\n",&Voltages[i]); if(show) printf("%5.2f \n",Voltages[i]);
//  for(i=0;i<Multiple-1;i++) {fscanf(in,"%e ",&Current[i]); if(show) printf("%5.2e ",Current[i]); }; 
//  fscanf(in,"%e\n",&Current[i]); if(show) printf("%5.2e\n",Current[i]);
//  for(i=0;i<Multiple-1;i++) {fscanf(in,"%f ",&Temperature[i]); if(show) printf("%5.1f ",Temperature[i]); }; 
//  fscanf(in,"%f\n",&Temperature[i]); if(show) printf("%5.1f\n",Temperature[i]);

// deltat*=1e9; t0*=1e9;
// if(Start==-1111) StartBin=t0; else StartBin=Start;
// EndBin=StartBin+deltat*Points; 
// if(show) printf("Start Bin=%5.2e, End Bin=%5.2e, Bins=%d\n",StartBin,EndBin,Bins);

//  for(i=0;i<Multiple;i++) 
//    {
//      sprintf(buff,"Measured WF %5.2f V",Voltages[i]);
//      printf("Histo Name = %s\n",buff);
//      new(entryp[i]) TH1F((const Char_t *)buff,(const Char_t *)buff,Bins,StartBin,EndBin);

//    }
// printf("Start loading the data\n");
// for(j=0;j<Points;j++) 
//   for(i=0;i<Multiple;i++)
//     {
//        if(i==Multiple-1) {fscanf(in,"%e\n",&data);} else {fscanf(in,"%e\t",&data);}
//        time=(Float_t)j*deltat+StartBin;
//       ((TH1F*)entryp[i])->Fill(time,(Double_t)data);
//       // if(i==Multiple-1) {printf("%e\n #=%d::::::",data,j);} else {printf("%e\t",data);}
//     }  

// fclose(in);
// if(show) printf("File:: %s read! \n",(const Char_t *)FileName); 



// //fclose(in);
// //ReadTCTForm(FileName,Bins,Start);
// }



MeasureWF::~MeasureWF()
{
  // Destructor
  if(pt!=NULL) delete pt;
  Clear();
#ifdef RHDB
  if(res!=NULL) delete res;
#endif
  //delete histo;
}

/////////////////////////////////////////////////////////////////////////////////////
#ifdef RHDB
void MeasureWF::DBResult(Char_t *UserName)
{
  TDatime dat;
  Char_t DateString[21];
  Char_t DateAnalyse[21];
  Char_t DeviceName[100];
  Char_t Side[100];
  Int_t code=0;
  sprintf(DateString,"%d-%d-%d %d:%d:%d",(Int_t) Date[2]+2000,(Int_t) Date[1],(Int_t) Date[0], (Int_t) Date[3],(Int_t) Date[4],(Int_t) Date[5]);
  DBGetString("Device:","Tester:",DeviceName);
  sprintf(DateAnalyse,"%d-%d-%d %d:%d:%d",dat.GetYear(),dat.GetMonth(),dat.GetDay(),dat.GetHour(),dat.GetMinute(),dat.GetSecond());
  DBGetString("Illumination_side:","Laser_wavelenght[nm]:",Side);
  if(!strcmp(Side,"Front")) code=10; else code=20;
  DBGetString("Laser_wavelenght[nm]:","Annealing_time:",Side); 
  if(!strcmp(Side,"670")) code+=0; 
  if(!strcmp(Side,"832")) code+=1; 
  if(!strcmp(Side,"1060")) code+=2; 
  if(!strcmp(Side,"alpha")) code+=3; 


  res=new HHRHDBResults(DeviceName,DateString);
  res->SetEvalDate(DateAnalyse);
  res->SetUser(UserName);

  Float_t maxT=-273,minT=1000,sumT=0;
  for(Int_t i=0;i<Temperature.GetSize();i++)
    {
      if(Temperature[i]!=0)
	{
	  maxT=Temperature[i]>maxT?Temperature[i]:maxT;
	  minT=Temperature[i]<minT?Temperature[i]:minT;
	  sumT+=Temperature[i];
	}
    }
  sumT/=Temperature.GetSize();
  DBSet(0,code);
  DBSet(1,sumT);
  DBSet(2,maxT-minT);
  DBSet(3,0);
  DBSet(8,Voltages[Voltages.GetSize()-1]);
}


Int_t MeasureWF::DBGetString(Char_t *tok1,Char_t *tok2,Char_t *devname)
{
  Int_t tok1e=Header.Index(tok1)+strlen(tok1)+1;
  Int_t tok2s=Header.Index(tok2)-1;
  //  printf("%d %d\n",tok1e,tok2s);
  strncpy(devname,&Header[tok1e],tok2s-tok1e);
  devname[tok2s-tok1e]='\0';
}
#endif
///////////////////////////////////////////////////////////////////////////////////////

void MeasureWF::AddHisto(Int_t index,Float_t voltage,TH1F *hisin)
{
  // Add histogram (waveform) 
  // 		Int_t Index     ;  Index of the waveform
  // 		Float_t Voltage ;  Voltage at Index 
  // 		TH1F*           ;  pointer to histogram of the waveform
  // This function is internally used for reading the measurement files
  if(index>Multiple || index<0) printf("Array out of bonds\n"); else
    {
      Voltages[index]=voltage;
      TClonesArray &entryp = *histo;
      new(entryp[index]) TH1F();
      hisin->Copy(*entryp[index]);
    }  
}

void MeasureWF::AddHisto(Float_t voltage,TH1F *hisin)
{
  // Add histogram (waveform) 
  // 	        Float_t Voltage ;  Voltage at Index 
  // 		TH1F*           ;  pointer to histogram of the waveform
  // This function is internally used for reading the measurement files. 
  // It is assumed that the array of waveforms is already initialized
  // with voltages specified
  Int_t index,i;
  for(i=0;i<Multiple;i++) if(Voltages[i]==voltage) 
    {
      index=i;
      TClonesArray &entryp = *histo;
      new(entryp[index]) TH1F();
      hisin->Copy(*entryp[index]);
    
    }
}

void MeasureWF::CompareHisto(TH1F *his,Float_t voltage)
{
  //  Compares measured histogram to any other histogram!
  //  Measured histogram is scaled to the input histogram!
  //                   TH1F *his       ;  input histogram
  // 		     Float_t voltage ;  histogram at voltage
  Int_t i;
  TH1F *ch=new TH1F();
  GetHistogram(voltage,ch); 
  ch->SetLineColor(0);
  his->DrawCopy();
  ch->Scale(his->GetMaximum()/ch->GetMaximum());
  ch->SetLineColor(1);
  ch->DrawCopy("SAME");
}

void MeasureWF::DrawTest(Int_t number)
{

    TH1F *his;
    his=(TH1F *)(histo->At(number));
    his->DrawCopy();
    his->SetLineColor(1); 
    his->SetXTitle("t[ns]");
    his->SetYTitle("I [V/50#Omega]");
    his->SetLabelSize(0.045,"X");
    his->SetLabelSize(0.045,"Y"); 
    // Legend(number,number,number);

}


void MeasureWF::Draw(Int_t number,Option_t *option,Float_t low,Float_t high)
{
  //  Draw waveform 
  //           Int_t number     ;  index of waveform 
  //           Option_t *option ;  graphic option (see TH1F)
  // 	     Float_t low      ;  Time window
  // 	     Float_t high     ;
  if(number<0 || number>=Multiple) printf("No such measurment!!\n"); 
  else {
    if(low!=-1111. || high!=-1111.) 
      ((TH1F *)histo->At(number))->GetXaxis()->SetRange(((TH1F *)histo->At(number))->GetXaxis()->FindBin(low),((TH1F *)histo->At(number))->GetXaxis()->FindBin(high));
    TH1 *his=((TH1F *)histo->At(number))->DrawCopy(option);

    his->SetLineColor(1); 
    his->SetXTitle("t[ns]");
    his->SetYTitle("I [V/50#Omega]");
    his->SetLabelSize(0.045,"X");
    his->SetLabelSize(0.045,"Y"); 
    // Legend(number,number,number);
  }
}



TGraph *MeasureWF::RiseTime(Float_t low,Float_t high,Int_t Start,Int_t End,Int_t Step,Int_t model,Int_t indsub)
{
  //  Draws multiple waveform on the same plot with legend
  //           Float_t low      ; Time window
  //           Float_t high     ;  
  // 	     Int_t Start      ; start index
  // 	     Int_t End        ; end index
  //	     Int_t Step       ; step (default=1) 
  //         Int_t model      ; positive scale in volts=2, deconvoluted pulse=1	
  // One of the most used functions
  Char_t v[5];
  Int_t i,maxi,mini,color,cii=0; 
  Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  Float_t max=0,min=0,valmax,valmin,temp;
  Elec *elTCT=new Elec(CAP,50,100e-9);
  TH1F *his,*hism,*subhis; 
  if(Start==-1) Start=0;
  if(End==-1) End=Multiple-1;
  for(i=Start;i<=End;i+=Step) { 
    valmax=((TH1F *)histo->At(i))->GetMaximum();
    valmin=((TH1F *)histo->At(i))->GetMinimum();
    if(valmax>max) {max=valmax; maxi=i;} 
    if(valmin<min) {min=valmin; mini=i;} 
  }
  if(max==0) maxi=End; //printf("maxi=%d",maxi);
  if(low!=-1111. || high!=-1111.) 
    ((TH1F *)histo->At(maxi))->GetXaxis()->SetRange(((TH1F *)histo->At(maxi))->GetXaxis()->FindBin(low),((TH1F *)histo->At(maxi))->GetXaxis()->FindBin(high));

  hism=((TH1F *)histo->At(maxi));

  if(model==1) 
    { his=new TH1F("Deconvoluted","Deconvoluted",hism->GetNbinsX(),hism->GetXaxis()->GetXmin(),hism->GetXaxis()->GetXmax()); 
      hism->Copy(*his); elTCT->Revpreamp((TH1F *)his,1e9);} 
    else his=hism;


  his->SetXTitle("t[ns]");
  his->SetYTitle("I [V/50#Omega]");
  his->GetXaxis()->SetTitleOffset(1.1);
  his->GetYaxis()->SetTitleOffset(1.1);
  his->SetLabelSize(0.040,"X");
  his->SetLabelSize(0.040,"Y");
  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
  TString title="TCT Measurement @ T=";title=title+v; title=title+" C";
  his->SetTitle((const char *)title);
  if(TMath::Abs(max/min)>0.2) his->SetMinimum(min*1.1);
  if(DrawMode) his->DrawCopy(); else his->Draw();
  // for(int ss=2500;ss<3000;ss++) printf("%d %f\n",ss,his->GetBinContent(ss)); 
  Legend((TH1 *)his,Start,End,Step,model);

  if(indsub!=-1111)
    {
        subhis=((TH1F *)histo->At(indsub)); 
	subhis->Scale(-1.);	
    }

  Float_t *gind=new Float_t [(End-Start)/Step];
  Float_t *rt=new Float_t [(End-Start)/Step];
  TF1 *f1=new TF1("p1","pol1",low,high);
  for(i=Start;i<=End;i+=Step)
    {
      //      color=i/7*40+i%7+1; 
      hism=((TH1F *)histo->At(i)); 
      gind[cii]=(Float_t) Voltages[i];
    
      //color=1;
      color=colori[cii];
      hism->SetLineColor((Color_t)color);
      if(model==1) { hism->Copy(*his); elTCT->Revpreamp((TH1F *)his,1e9);} else his=hism; 
      if(indsub!=-1111) {his->Add(subhis);}
      // if(DrawMode) his->DrawCopy("SAME"); else his->Draw("SAME");
      his->Fit("p1","RQN");
      rt[cii]=f1->GetParameter(1);
      
      cii++;      
    } 

  if(model==1) model=2;
  //while(!getchar());   
  delete elTCT;

  TGraph *grRiseTime=new TGraph((End-Start)/Step,gind,rt);
  grRiseTime->Draw("AL*");
  return grRiseTime;

}


TGraph *MeasureWF::AnalysePSTCT(Int_t Start,Int_t End,Int_t Step,Float_t X0,Float_t limit,Int_t lrbin, Int_t mode, Int_t sign)
{
  //  Draws multiple waveform on the same plot with legend
  // 	     Int_t Start      ; start index
  // 	     Int_t End        ; end index
  //	     Int_t Step       ; step (default=1) 
  //         Float_t limit    ; treshold value
  // One of the most used functions

  Int_t i,maxi,mini,color,cii=0,low,hi; 
  // Float_t max=0,min=0,valmax,valmin,temp;
  Int_t NumPoints;

  TH1F *his,*hism,*subhis; 
  TGraph *gr; 
 if(Start==-1) Start=0;
  if(End==-1) End=Multiple-1;

 
  NumPoints=(End-Start)/Step;
  //  TH1F *VelProfile=new TH1F ("PosVsTime","PosVsTime",NumPoints,Voltages[Start],Voltages[End]);


  Float_t *gind=new Float_t [NumPoints];
  Float_t *rt=new Float_t [NumPoints];
  Float_t *velo=new Float_t [NumPoints];
  Float_t *efield=new Float_t [NumPoints];
  
  TF1 *fit=new TF1("fit","pol1",-1000,1000);
  TF1 *fit2=new TF1("fit2","pol2",-1000,1000);
  TF1 *fit3=new TF1("fit3","pol3",-1000,1000);

  Int_t minbin;
  Float_t vel;
  double eps=1,y1,y2,y3,a,b,sum=0;
  int nosteps=0,ss=0;

  for(i=Start;i<=End;i+=Step)
    {
      //      color=i/7*40+i%7+1; 
      hism=((TH1F *)histo->At(i)); 

      for(int s=1;s<=hism->GetNbinsX();s++)
	{
	  if(hism->GetBinContent(s)<limit) minbin=s;
	}

      fit->SetRange(hism->GetBinCenter(minbin-2),hism->GetBinCenter(minbin+2));
      hism->Fit("fit","RNQ");
      rt[cii]=((limit-fit->GetParameter(0))/fit->GetParameter(1));

      gind[cii]=(Float_t) Voltages[i]-X0;
      cii++;      
    } 

  TGraph *grRiseTime=new TGraph(NumPoints,rt,gind);


   fit->SetRange(rt[0],rt[NumPoints]);
   grRiseTime->Fit("fit3","RQ");
   fit2->SetParameter(0,fit3->GetParameter(1));
   fit2->SetParameter(1,2*fit3->GetParameter(2));
   fit2->SetParameter(2,3*fit3->GetParameter(3));

  for(ss=0;ss<NumPoints;ss++) 
    {
   //    if(ss>=lrbin) low=ss-lrbin; else low=0;
//       if(ss+lrbin>=NumPoints) hi=NumPoints-1; else hi=ss+lrbin;
//       fit->SetRange(rt[low],rt[hi]);
//       grRiseTime->Fit("fit","RNQ");
//       velo[ss]=fit->GetParameter(1);
         velo[ss]=fit2->Eval(rt[ss]);
    }
  
  TGraph *grVeloVsTime=new TGraph(NumPoints,gind,velo); 
  Float_t charge;
  switch(mode)
    {
    case 0: 
      gr=grRiseTime; break;
    case 1: 
      gr=grVeloVsTime; break; 
    default: 
      if(mode==2) charge=-1; else charge=1;
      printf(" Charge=%f\n sign=%d\n",charge,sign);
      for(ss=0; ss<NumPoints; ss++)
       {
	 vel=sign*velo[ss]*1e5; printf("vel=%f %f\n",vel,charge);
          a=0;
          b=40*1e4; //40 V / mikron
          eps=TMath::Abs(b-a);
	  while(eps>0.01 && nosteps < 1e5)
	    {
	      y1=vel-Mobility(a,300,charge,1e12, 1)*a;
	      y2=vel-Mobility(b,300,charge,1e12, 1)*b;
	      y3=vel-Mobility((a+b)/2,300,charge,1e12, 1)*(a+b)/2;
	      if(y1*y2>0 && nosteps==0) {printf("Velocity to high!!!\n"); b=0; a=0; break;}
	      //     printf("y1=%f , y2=%f, y3=%e , a=%e, b=%e, T=%f \n",y1,y2,y3,a,b,Temp);
	      if(y1*y3<0) b=(a+b)/2.; else a=(a+b)/2;
	      eps=TMath::Abs(a-b); //printf("no steps = %d vel = %f %f\n",nosteps,vel,eps);
	      nosteps++;

/* 	    } */
	  //  printf("i=%d, vel=%f, E=%f, eps=%f, nosteps=%d\n",i,vel,(a+b)/2,eps,nosteps);
	    }
	  efield[ss]=a;
	  sum+=a;
	}
     printf("Integral=%f\n",sum);
     TGraph *grEf=new TGraph(NumPoints,gind,efield); 
     gr=grEf;
      break;
    }
  gr->Draw("AL*");
  return gr;
}




TGraph *MeasureWF::PosTime(Float_t low,Float_t high,Int_t Start,Int_t End,Int_t Step,Int_t model,Int_t indsub)
{
  //  Draws multiple waveform on the same plot with legend
  //           Float_t low      ; Time window
  //           Float_t high     ;  
  // 	     Int_t Start      ; start index
  // 	     Int_t End        ; end index
  //	     Int_t Step       ; step (default=1) 
  //         Int_t model      ; positive scale in volts=2, deconvoluted pulse=1	
  // One of the most used functions
  Char_t v[5];
  Int_t i,maxi,mini,color,cii=0; 
  Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  Float_t max=0,min=0,valmax,valmin,temp;
  float relparmin=1e5,relpar;
  Elec *elTCT=new Elec(CAP,50,100e-9);
  TH1F *his,*hism,*subhis; 
  if(Start==-1) Start=0;
  if(End==-1) End=Multiple-1;
  for(i=Start;i<=End;i+=Step) { 
    valmax=((TH1F *)histo->At(i))->GetMaximum();
    valmin=((TH1F *)histo->At(i))->GetMinimum();
    if(valmax>max) {max=valmax; maxi=i;} 
    if(valmin<min) {min=valmin; mini=i;} 
  }
  if(max==0) maxi=End; //printf("maxi=%d",maxi);
  if(low!=-1111. || high!=-1111.) 
    ((TH1F *)histo->At(maxi))->GetXaxis()->SetRange(((TH1F *)histo->At(maxi))->GetXaxis()->FindBin(low),((TH1F *)histo->At(maxi))->GetXaxis()->FindBin(high));
  printf("KURAC 1\n");
  hism=((TH1F *)histo->At(maxi));

  if(model==1) 
    { his=new TH1F("Deconvoluted","Deconvoluted",hism->GetNbinsX(),hism->GetXaxis()->GetXmin(),hism->GetXaxis()->GetXmax()); 
      hism->Copy(*his); elTCT->Revpreamp((TH1F *)his,1e9);} 
    else his=hism;


  his->SetXTitle("t[ns]");
  his->SetYTitle("I [V/50#Omega]");
  his->GetXaxis()->SetTitleOffset(1.1);
  his->GetYaxis()->SetTitleOffset(1.1);
  his->SetLabelSize(0.040,"X");
  his->SetLabelSize(0.040,"Y");
  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
  TString title="TCT Measurement @ T=";title=title+v; title=title+" C";
  his->SetTitle((const char *)title);
  if(TMath::Abs(max/min)>0.2) his->SetMinimum(min*1.1);
  if(DrawMode) his->DrawCopy(); else his->Draw();
  // for(int ss=2500;ss<3000;ss++) printf("%d %f\n",ss,his->GetBinContent(ss)); 
  Legend((TH1 *)his,Start,End,Step,model);

  if(indsub!=-1111)
    {
        subhis=((TH1F *)histo->At(indsub)); 
	subhis->Scale(-1.);	
    }

  Float_t *gind=new Float_t [(End-Start)/Step];
  Float_t *rt=new Float_t [(End-Start)/Step];
  TF1 *f2=new TF1("p2","pol2",low,high);
  for(i=Start;i<=End;i+=Step)
    {
      //      color=i/7*40+i%7+1; 
      hism=((TH1F *)histo->At(i)); 
      gind[cii]=(Float_t) Voltages[i];
    
      //color=1;
      color=colori[cii];
      hism->SetLineColor((Color_t)color);
      if(model==1) { hism->Copy(*his); elTCT->Revpreamp((TH1F *)his,1e9);} else his=hism; 
      if(indsub!=-1111) {his->Add(subhis);}
      // if(DrawMode) his->DrawCopy("SAME"); else his->Draw("SAME");
      valmin=((TH1F *)histo->At(i))->GetBinCenter( ((TH1F *)histo->At(i))->GetMinimumBin() );

      relparmin=1e5;
  //     for(int jj=0;jj<3;jj++)
//       for(int ii=0;ii<6;ii++)
// 	{
// 	  switch(jj)
// 	    {
//             case 2: f2->SetRange(valmin-2-(float) ii/3.,valmin+2+(float) ii/3.);  break;
//             case 1: f2->SetRange(valmin-2-(float) ii/3.,valmin+2); break;
//             case 0: f2->SetRange(valmin-2,valmin+2+(float) ii/3.); break;
// 	    }
// 	  his->Fit("p2","RQ");
// 	  relpar=f2->GetChisquare()/f2->GetNDF();
// 	  //	  printf("ii=%d,jj=%d,%e,%e\n",ii,jj,relpar,relparmin);
// 	  if(relpar<relparmin) 
//               {
//                relparmin=relpar; 
//                rt[cii]=-f2->GetParameter(1)/(2*f2->GetParameter(2)); 
//                printf("cii=%d ii=%d,jj=%d,%e,%e (%f %d)\n",cii,ii,jj,relpar,relparmin,f2->GetChisquare(),f2->GetNDF() ); }
// 	}
//       printf("\n");
      if(rt[cii-1]>7 && rt[cii-1]<10)
      f2->SetRange(valmin-3.8,valmin+3.8);
      else 
	if ( rt[cii-1]>10)
      f2->SetRange(valmin-4,valmin+4);
      	else
      f2->SetRange(valmin-2.8,valmin+2.8);
      his->Fit("p2","R");
      rt[cii]=-f2->GetParameter(1)/(2*f2->GetParameter(2));
      //      rt[cii]=valmin;
      printf("%d, %f %d\n",cii,f2->GetChisquare(),f2->GetNDF() );
      cii++;      
    } 

  if(model==1) model=2;
  //while(!getchar());   
  delete elTCT;

  TGraph *grRiseTime=new TGraph((End-Start)/Step,rt,gind);
  grRiseTime->Draw("AL*");
  return grRiseTime;

}


void MeasureWF::DrawMulti(Float_t low,Float_t high,Int_t Start,Int_t End,Int_t Step,Int_t model,Int_t indsub)
{
  //  Draws multiple waveform on the same plot with legend
  //           Float_t low      ; Time window
  //           Float_t high     ;  
  // 	     Int_t Start      ; start index
  // 	     Int_t End        ; end index
  //	     Int_t Step       ; step (default=1) 
  //         Int_t model      ; positive scale in volts=2, deconvoluted pulse=1	
  //         Int_t indsub      ; substract wafeforw index	
  // One of the most used functions
  Char_t v[5];
  Int_t i,maxi,mini,color,cii=0; 
  Int_t colori[]={1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,6,7,1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  Float_t max=0,min=0,valmax,valmin,temp;
  Elec *elTCT=new Elec(CAP,50,100e-9);
  TH1F *his,*hism,*subhis; 
  if(Start==-1) Start=0;
  if(End==-1) End=Multiple-1;
  for(i=Start;i<=End;i+=Step) { 
    valmax=((TH1F *)histo->At(i))->GetMaximum();
    valmin=((TH1F *)histo->At(i))->GetMinimum();
    if(valmax>max) {max=valmax; maxi=i;} 
    if(valmin<min) {min=valmin; mini=i;} 
  }
  if(max==0) maxi=End; //printf("maxi=%d",maxi);
  if(low!=-1111. || high!=-1111.) 
    ((TH1F *)histo->At(maxi))->GetXaxis()->SetRange(((TH1F *)histo->At(maxi))->GetXaxis()->FindBin(low),((TH1F *)histo->At(maxi))->GetXaxis()->FindBin(high));

  hism=((TH1F *)histo->At(maxi));

  if(model==1) 
    { his=new TH1F("Deconvoluted","Deconvoluted",hism->GetNbinsX(),hism->GetXaxis()->GetXmin(),hism->GetXaxis()->GetXmax()); 
      hism->Copy(*his); elTCT->Revpreamp((TH1F *)his,1e9);} 
  else his=hism;


  his->SetXTitle("t[ns]");
  his->SetYTitle("I [V/50#Omega]");
  his->GetXaxis()->SetTitleSize(0.055);
  his->GetYaxis()->SetTitleSize(0.055);
  his->GetXaxis()->SetTitleOffset(1.1);
  his->GetYaxis()->SetTitleOffset(1.1);
  his->SetLabelSize(0.05,"X");
  his->SetLabelSize(0.05,"Y");
  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
  TString title="TCT Measurement @ T=";title=title+v; title=title+" C";
  his->SetTitle((const char *)title);
  if(TMath::Abs(max/min)>0.2) his->SetMinimum(min*1.1);
  if(DrawMode) his->DrawCopy(); else his->Draw();
  // for(int ss=2500;ss<3000;ss++) printf("%d %f\n",ss,his->GetBinContent(ss)); 
  Legend((TH1 *)his,Start,End,Step,model);

  if(indsub!=-1111)
    {
        subhis=((TH1F *)histo->At(indsub)); 
	subhis->Scale(-1.);	
    }


  for(i=Start;i<=End;i+=Step)
    {
      //      color=i/7*40+i%7+1; 
      hism=((TH1F *)histo->At(i)); 
      //     color=1;
         color=colori[cii];
      hism->SetLineColor((Color_t)color);
      if(model==1) { hism->Copy(*his); elTCT->Revpreamp((TH1F *)his,1e9);} else his=hism; 
      if(indsub!=-1111) {his->Add(subhis);}
      if(DrawMode) his->DrawCopy("SAME"); else his->Draw("SAME");
      cii++;      
    } 

  if(model==1) model=2;
  //while(!getchar());   
  delete elTCT;
}


Double_t sg(Double_t *x, Double_t *par)
{
  Double_t dvg=0;
  dvg=par[0]*TMath::Exp(- TMath::Power((*x-par[1]),2)/par[2])+par[3];
  return dvg;;
}


Float_t MeasureWF::Integral(Int_t i,Float_t mint, Float_t maxt)
{
  // Get waveform integral in time window
  // 	Int_t i      ; index
  //      Float_t mint ; Time window
  //      Float_t maxt ;
  // One of the most used functions
  Int_t mintime;
  Int_t maxtime;

  TH1F *his=((TH1F *)histo->At(i));
  if (maxt==-1111) maxtime=his->GetNbinsX()-1; else maxtime=his->GetXaxis()->FindBin(maxt);
  if (mint==-1111) mintime=1; else mintime=his->GetXaxis()->FindBin(mint);
 
  return(his->Integral(mintime,maxtime)*his->GetBinWidth(1));      
}

Float_t MeasureWF::Integral(Float_t y,Float_t mint, Float_t maxt)
{
  // Get waveform integral in time window (uses voltage insted of index)
  Int_t index;
  for(Int_t i=0;i<Multiple;i++) if(Voltages[i]==y) index=i; 
  return(Integral(index,mint,maxt));
}



//___________________________________________________________________________________________________-
void MeasureWF::GetIntegral(Float_t *x,Float_t *y,Float_t scale,Float_t mint, Float_t maxt)
{
  // Gets integrals in certain time window
  // 		Float_t  *x  ;  array of voltages
  // 		Float_t  *y  ;  array of integrals
  // 		Float_t  scale ;  scaling factor
  // 		Float_t mint;   Time window
  // 		Float_t maxt;

  Int_t i;
  Int_t mintime;
  Int_t maxtime;

  TH1F *his;
  for(i=0;i<Multiple;i++)
    {
      his=((TH1F *)histo->At(i));
      if (maxt==-1111) maxtime=his->GetNbinsX()-1; else maxtime=his->GetXaxis()->FindBin(maxt);
      if (mint==-1111) mintime=1; else mintime=his->GetXaxis()->FindBin(mint);
      //	printf("%d ,%d\n",mintime,maxtime);
      y[i]=his->Integral(mintime,maxtime)*his->GetBinWidth(1)/scale;
      if(Voltages[i]!=0 || Voltages[i+1]!=0) x[i]=Voltages[i]; else x[i]=(Float_t)i;
    }
}
//___________________________________________________________________________________________________-
void MeasureWF::GetIntegral(Float_t *y,Float_t scale,Float_t mint, Float_t maxt)
{
  // Get Integral values only
  Float_t *x=new Float_t[Multiple];
  GetIntegral(x,y,scale,mint,maxt);
}
//___________________________________________________________________________________________________-




TGraph *MeasureWF::CCE(Float_t mint, Float_t maxt,Int_t model,Int_t Show)
{
  // Get integral (charge) plot 
  //                Float_t mint  ; Time Window
  //                Float_t maxt  ; 
  //		    Int_t model   ; lin-lin=0  scale  sqrt-lin=1 abs(lin)-lin=2
  //		    Int_t Show    ; show graph
  Char_t v[6];
  Int_t i;
  Float_t *integral=new Float_t [Multiple];
  Float_t *index=new Float_t [Multiple];

  GetIntegral(index,integral,1,mint,maxt);

  if(model==1) for(i=0;i<Multiple;i++) index[i]=TMath::Sqrt(TMath::Abs(index[i]));
  if(model==2) for(i=0;i<Multiple;i++) index[i]=TMath::Abs(index[i]);

  TGraph *gr=new TGraph(Multiple,index,integral);
  gr->SetLineColor(2);
  gr->SetLineWidth(4);
  gr->SetMarkerColor(4);
  gr->SetMarkerStyle(21);

  //Get mean tempearature written in the graph title
  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
  TString title="Charge vs. Voltage @ T=";title=title+v; title=title+" C";
  gr->SetTitle((const char *)title); 
   
  if(Show) 
    {
      gr->Draw("APL");
      if(model!=1) gr->GetHistogram()->SetXTitle("U[V]"); else gr->GetHistogram()->SetXTitle("Sqrt U[ Sqrt V]");
      gr->GetHistogram()->SetYTitle("Charge[arb.]");
      gr->GetHistogram()->Draw();
      gr->Draw("APL");
    }
  return(gr);
}
//___________________________________________________________________________________________________-

TGraph *MeasureWF::CCEFit(Float_t mint, Float_t maxt,Float_t U1,Float_t U2,Int_t model,Int_t Show)
{
  // Get integral (charge) plot 
  //                Float_t mint  ; Time Window
  //                Float_t maxt  ; 
  //		    Int_t model   ; lin-lin=0  scale  sqrt-lin=1 abs(lin)-lin=2
  //		    Int_t Show    ; show graph
  Char_t v[6];
  Int_t i;
  Float_t *integral=new Float_t [Multiple];
  Float_t *index=new Float_t [Multiple];

  Double_t par[2]={0.5,1};
  TF1 *func=new TF1("FitF","[0]*x^[1]",U1,U2);
  func->SetParameters(par);

  GetIntegral(index,integral,1,mint,maxt);

  if(model==1) for(i=0;i<Multiple;i++) index[i]=TMath::Sqrt(TMath::Abs(index[i]));
  if(model==2) for(i=0;i<Multiple;i++) index[i]=TMath::Abs(index[i]);

  TGraph *gr=new TGraph(Multiple,index,integral);
  gr->SetLineColor(2);
  gr->SetLineWidth(4);
  gr->SetMarkerColor(4);
  gr->SetMarkerStyle(21);

  //Get mean tempearature written in the graph title
  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
  TString title="Charge vs. Voltage @ T=";title=title+v; title=title+" C";
  gr->SetTitle((const char *)title); 
   
  if(Show) 
    {
      gr->Draw("APL");
      if(model!=1) gr->GetHistogram()->SetXTitle("U[V]"); else gr->GetHistogram()->SetXTitle("Sqrt U[ Sqrt V]");
      gr->GetHistogram()->SetYTitle("Charge[arb.]");
      gr->GetHistogram()->Draw();
      gr->Draw("APL");
      gr->Fit("FitF","R");
    }
  return(gr);
}



//___________________________________________________________________________________________________-

void MeasureWF::Legend(TH1 *ch, Int_t start, Int_t end, Int_t Step,Int_t model)
{
  // Draw Legen (DrawMulti)
  //	       TH1F *ch    ; histogam 
  //	       Int_t start ; start index
  //	       Int_t end   ; end index
  //	       Int_t model ; positive scale in volts=2
  Float_t minx,miny,maxy,maxx,x1,x2,y1,y2;
  Char_t title[30];

  Int_t color,cii=0;
  Int_t colori[]={1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,6,7,1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  TText *text;

  minx=ch->GetXaxis()->GetBinCenter(ch->GetXaxis()->GetFirst());
  maxx=ch->GetXaxis()->GetBinCenter(ch->GetXaxis()->GetLast());
  miny=ch->GetMinimum();
  maxy=ch->GetMaximum();

  x1=(maxx-minx)*0.6+minx; 
  x2=(maxx-minx)*0.9+minx;
  y2=(maxy-miny)*0.35+miny;
  y1=(maxy-miny)*0.95+miny;

  //printf("coords: x1=%f y1=%f x2=%f y2=%f\n",x1,y1,x2,y2);

  if(pt!=NULL) delete pt;
  pt=new TPaveText(x1,y1,x2,y2);
  for(Int_t i=start;i<=end;i+=Step)
    {
      if(model!=0) 
      sprintf(title,"%s %d %s",prefix,(Int_t)TMath::Abs(GetVoltage(i)),suffix);
      else 
      sprintf(title,"%s %d %s",prefix,(Int_t) GetVoltage(i),suffix);
      //   color=i/7*40+i%7+1; 
      color=colori[cii];
      //       color=1;
      text=pt->AddText(title);
      text->SetTextColor(color);
      text->SetTextSize(0.05);
      cii++;
    }
  pt->Draw();
}





Float_t MeasureWF::GetFDV(Elec *el,Float_t x0,Float_t x1,Float_t x2, Float_t x3,Int_t model,Int_t show)
{
  //	Calculates FDV using CCE plot 
  //		     Elec *el   ;  electronics used (See Elec)
  //		     See GetFDV(Float_t, Float_t .... ), for the rest of information
  Float_t result;
  TGraph *gr=CCEE(el,-1111,model,show);
  TF1 *p1=new TF1("premica1","pol1",x0,x1);
  TF1 *p2=new TF1("premica2","pol1",x2,x3);

  gr->Fit("premica2","RQ");
  gr->Fit("premica1","RQ");
  result=(p2->GetParameter(0)-p1->GetParameter(0))/(p1->GetParameter(1)-p2->GetParameter(1));
  if(model==1) result*=result;

  if(show)
    {
      TLine *line1=new TLine(x0,p1->GetParameter(0)+p1->GetParameter(1)*x0,x2,p1->GetParameter(0)+p1->GetParameter(1)*x2);
      TLine *line2=new TLine(x1,p2->GetParameter(0)+p2->GetParameter(1)*x1,x3,p2->GetParameter(0)+p2->GetParameter(1)*x3);
      line1->SetLineColor(1);
      line1->SetLineWidth(2);
      line2->SetLineColor(1);
      line2->SetLineWidth(2);
      line2->Draw();
      line1->Draw();
    } 
  else 
    {
      delete p1;
      delete p2;
      delete gr;
    }

  return(result);
}



Float_t MeasureWF::GetFDV(Float_t low, Float_t hi,Float_t x0,Float_t x1,Float_t x2, Float_t x3,Int_t model,Int_t show)
{
  //	Calculates FDV using integral of current
  //		     Float_t low;  Time window
  //		     Float_t hi ;
  //		     Float_t x0 ;  first point of the first line
  //		     Float_t x1 ;  second point of the first line
  //		     Float_t x2 ;  first point of the second line
  //		     Float_t x3 ;  second point of the second line
  //		     Int_t model;  the model for FDV calculation (see CCE)
  //		     Int_t show ;  show plot
  Float_t result;
  TGraph *gr=CCE(low,hi,model,show);
  // if(x2>gr->GetX()[gr->GetN()]) {x2=gr->GetX()[gr->GetN()-1]; x2=gr->GetX()[gr->GetN()]; } 
  TF1 *p1=new TF1("premica1","pol1",x0,x1);
  TF1 *p2=new TF1("premica2","pol1",x2,x3);
  gr->Fit("premica2","RQ");
  gr->Fit("premica1","RQ");

  result=(p2->GetParameter(0)-p1->GetParameter(0))/(p1->GetParameter(1)-p2->GetParameter(1));
  if(model==1) result*=result;
 
  if(show)
    {
      TLine *line1=new TLine(x0,p1->GetParameter(0)+p1->GetParameter(1)*x0,x2,p1->GetParameter(0)+p1->GetParameter(1)*x2);
      TLine *line2=new TLine(x1,p2->GetParameter(0)+p2->GetParameter(1)*x1,x3,p2->GetParameter(0)+p2->GetParameter(1)*x3);
      line1->SetLineColor(1);
      line1->SetLineWidth(2);
      line2->SetLineColor(1);
      line2->SetLineWidth(2);
      line2->Draw(); 
      line1->Draw();
    }
  else
    {
      delete p1;
      delete p2;
      delete gr;
    } 

#ifdef RHDB
  Float_t IatVfd,QatVfd;
  Float_t X1,X2; 
  Int_t i;
  if(res!=NULL)
    {
      DBSet(4,result);
      for(i=0;i<Voltages.GetSize()-2;i++)
	if(Voltages[i]<=result && Voltages[i+1]>result) 
	  IatVfd=Current[i]+(result-Voltages[i])*(Current[i+1]-Current[i])/(Voltages[i+1]-Voltages[i]);      
 
      for(i=0;i<gr->GetN()-2;i++)
	{
	  X1=(gr->GetX())[i];
	  X2=(gr->GetX())[i+1];
	  if(model==1) {X1*=X1; X2*=X2;}
	  if(X1<=result && X2>result) 
	    QatVfd=(gr->GetY())[i]+(result-X1)*((gr->GetY())[i+1]-(gr->GetY())[i])/(X2-X1); 
	}
      X1=(gr->GetX())[gr->GetN()-1];
      if(model==1) X1*=X1;

      DBSet(5,IatVfd*1e6);
      DBSet(6,hi-low);
      DBSet(7,QatVfd);
      DBSet(8,X1);
      DBSet(9,(gr->GetY())[gr->GetN()-1]);
    } 
#endif

  return(result);
}

TGraph *MeasureWF::Tau(Elec *el,Float_t tau,Int_t start, Int_t end,Float_t mintime, Float_t maxtime,Int_t Show,Int_t model,TArrayI exlist)
{
  // Calcultaes rendered integrals above FDV with the trapping time ; waveform is deconvoluted first 
  //			Elec *el        ;  electronics
  //			Float_t tau     ;  trapping time constant
  //			Int_t start     ;  start index
  //			Int_t end       ;  end index
  //			Float_t mintime ;  Time window
  //			Float_t maxtime ; Time window
  //			Int_t Show      ; show graph
  //			Int_t model     ; 
  //                    TArrayI exlist  ; list of excluded points = 0

  Int_t i,j;
  Int_t maxit;
  Int_t minit;
  Float_t *mint=new Float_t [end-start+1];
  Float_t *maxt=new Float_t [end-start+1];
  for( i=0;i<end-start+1;i++) {mint[i]=mintime; maxt[i]=maxtime;};
  Float_t *I=new Float_t[end-start+1];
  Float_t *Ib=new Float_t[end-start+1];
  Float_t *V=new Float_t[end-start+1];
  TH1F *his=new TH1F();
  for(i=start;i<=end;i++)
    {
      GetHistogram(i,his);
      el->Revpreamp(his,1e9);
      maxit=his->GetXaxis()->FindBin(maxt[i-start]);
      minit=his->GetXaxis()->FindBin(mint[i-start]);
      Ib[i-start]=his->Integral(minit,maxit)*his->GetBinWidth(1);
      for(Int_t j=minit;j<maxit;j++) 
	his->SetBinContent(j,his->GetBinContent(j)*TMath::Exp((his->GetBinCenter(j)-his->GetBinCenter(minit))/tau));
      I[i-start]=his->Integral(minit,maxit)*his->GetBinWidth(1);
      //   V[i-start]=model==0?Voltages[i]:-Voltages[i];   
      V[i-start]=Voltages[i];   
      if(Show==1) 
	if(i==start) 
	  { 
	    his->SetMaximum(2*his->GetMaximum()); 
	    his->GetXaxis()->SetRange(minit,maxit); 
	    his->SetTitle("Corrected induced current pulse shapes");
	    his->SetXTitle("t[ns]");
	    his->SetYTitle("I [V/50#Omega]");
	    his->DrawCopy();
	    Legend((TH1 *)his,start,end,1,model);
	  } else  
	  his->DrawCopy("SAME");
    }
  TGraph *gr=new TGraph(end-start+1,V,I);
  TGraph *gr1=new TGraph(end-start+1,V,Ib);
  gr1->SetMarkerStyle(22);
  gr->SetMarkerStyle(21);
 
  if(exlist.GetSize()!=0) 
    for(i=0;i<exlist.GetSize();i++) gr->RemovePoint(exlist[i]-start-i); 
 
  if(Show==0) 
    {
      gr->Draw("APL");
      gr->GetHistogram()->SetXTitle("U[V]");
      gr->GetHistogram()->SetYTitle("Corrected charge [arb.]");
      gr->GetHistogram()->Draw();
      gr->Draw("APL");
      gr1->Draw("LP");
    }
  return(gr);
}

TGraph *MeasureWF::Tau(Float_t tau,Int_t start, Int_t end,Float_t *mint, Float_t *maxt,Int_t Show,Int_t model, TArrayI exlist)
{
  // Calcultaes rendered integrals above FDV with the trapping time
  //			Float_t tau     ;  trapping time constant
  //			Int_t start     ;  start index
  //			Int_t end       ;  end index
  //			Float_t *mint   ; Time windows for each separate measurement
  //			Float_t *maxt   ; 
  //			Int_t Show      ; show graph
  //			Int_t model     ; graphs with positive voltages =1
  //                    TArrayI exlist  ; list of excluded points = 0
  Int_t i,j;
  Int_t maxit;
  Int_t minit;
  Float_t *I=new Float_t[end-start+1];
  Float_t *Ib=new Float_t[end-start+1];
  Float_t *V=new Float_t[end-start+1];
  TH1F *his=new TH1F();
  for(i=start;i<=end;i++)
    {
      GetHistogram(i,his);
      maxit=his->GetXaxis()->FindBin(maxt[i-start]);
      minit=his->GetXaxis()->FindBin(mint[i-start]);
      Ib[i-start]=his->Integral(minit,maxit)*his->GetBinWidth(1);
      for(Int_t j=minit;j<maxit;j++) 
	his->SetBinContent(j,his->GetBinContent(j)*TMath::Exp((his->GetBinCenter(j)-his->GetBinCenter(minit))/tau));
      I[i-start]=his->Integral(minit,maxit)*his->GetBinWidth(1);
      V[i-start]=model==0?Voltages[i]:-Voltages[i];   
      if(Show==1) 
	if(i==start) 
	  { his->SetMaximum(2*his->GetMaximum()); 
	    his->GetXaxis()->SetRange(minit,maxit); 
	    his->SetTitle("Corrected induced current pulse shapes");
	    his->SetXTitle("t[ns]");
	    his->SetYTitle("I [V/50#Omega]");
	    his->DrawCopy();
	    Legend((TH1 *)his,start,end,1,2);
	  } else  
	  his->DrawCopy("SAME");
    }
  if(model==2) 
    { Float_t norm=0;
      for(j=0;j<=end-start;j++) norm+=I[j]; norm=norm/j;
      for(j=0;j<=end-start;j++) I[j]=Ib[j]/norm;; 
    }
  TGraph *gr=new TGraph(end-start+1,V,I);
  TGraph *gr1=new TGraph(end-start+1,V,Ib);
  gr1->SetMarkerStyle(22);
  gr->SetMarkerStyle(21);

  //exclude those channels on the list GK 6.11.2007
 if(exlist.GetSize()!=0) 
   for(i=0;i<exlist.GetSize();i++) gr->RemovePoint(exlist[i]-start-i);

  if(Show==0) 
    {

      gr->Draw("APL");
      gr->GetHistogram()->SetXTitle("U[V]");
      if(model!=2) gr->GetHistogram()->SetYTitle("Corrected Charge"); else 
	gr->GetHistogram()->SetYTitle("CCE");
      gr->GetHistogram()->Draw();
      gr->Draw("APL");
      if(model!=2)  gr1->Draw("LP");
    }
  return(gr);
}

TGraph *MeasureWF::Tau(Float_t tau,Int_t start, Int_t end,Float_t mint, Float_t maxt,Int_t Show,Int_t model,TArrayI exlist)
{
  // Calcultaes rendered integrals above FDV with the trapping time
  //			Float_t tau     ;  trapping time constant
  //			Int_t start     ;  start index
  //			Int_t end       ;  end index
  //			Float_t mint    ; Time window 
  //			Float_t maxt    ; 
  //			Int_t Show      ; show graph
  //			Int_t model     ; positive voltage scale graph=2
  //                    TArrayI exlist  ; list of excluded points = 0
  Float_t *mintime=new Float_t [end-start+1];
  Float_t *maxtime=new Float_t [end-start+1];
  for(Int_t i=0;i<end-start+1;i++) {mintime[i]=mint; maxtime[i]=maxt;};
  return(Tau(tau,start,end,mintime,maxtime,Show,model,exlist));
}


TGraph *MeasureWF::TauVolt(Float_t taus, Float_t taue, Int_t start, Int_t end,Float_t mint, Float_t maxt,Int_t res,Int_t model)
{
  // Calcultaes rendered integrals above FDV with the trapping time. The output is plot Integral vs voltage @ diff. tau.
  //			Float_t taus    ; Time interval
  //			Float_t taue    ; 
  //			Int_t start     ;  start index
  //			Int_t end       ;  end index
  //			Float_t mint    ; Time window 
  //			Float_t maxt    ; 
  //			Int_t res       ; how many steps per nsh
  //			Int_t model     ; see VoltTau
  if(res<1) res=1;
  TGraph *gr;
  Int_t k=0,i=0,minit,maxit;
  Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  //Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  Float_t tau=0;
  Float_t *Charge=new Float_t [(Int_t)(taue-taus)*res+1];
  Float_t *Voltage=new Float_t [(Int_t)(taue-taus)*res+1];
  Float_t *Time=new Float_t [(Int_t)(taue-taus)*res+1];
  Elec *elTCT=new Elec(CAP,50,100e-9);
  TH1F *his=new TH1F();

  for(i=start;i<=end;i++)
    {
      k=0;
      for(tau=taus;tau<=taue;tau+=1/(Float_t)res)
	{
	  GetHistogram(i,his);
	  if(model<0) elTCT->Revpreamp(his,1e9);
	  maxit=his->GetXaxis()->FindBin(maxt);
	  minit=his->GetXaxis()->FindBin(mint);
	  for(Int_t j=minit;j<maxit;j++) 
	    //  his->SetBinContent(j,his->GetBinContent(j)*TMath::Exp((his->GetBinCenter(j)-his->GetBinCenter(minit)))/tau);
	    his->SetBinContent(j,his->GetBinContent(j)*TMath::Exp((his->GetBinCenter(j)-his->GetBinCenter(minit))/tau));
	  Charge[k]=his->Integral(minit,maxit)*his->GetBinWidth(1);
	  Time[k]=tau;   
	  k++; his->Reset();
	}

      if(i==start) 
	{
	  gr=new TGraph(k,Time,Charge);
	  if(TMath::Abs(model)==2) gr->SetLineColor(colori[i-start]); else gr->SetLineColor(1); 
	  gr->Draw("AL"); 
	  gr->SetTitle("Corrected charge vs. effective trapping time ");
	  gr->GetHistogram()->SetXTitle("#tau_{tr} [ns]");
	  gr->GetHistogram()->SetYTitle("Corrected charge [arb.]");
	  gr->GetHistogram()->Draw();	
	  gr->DrawGraph(k,Time,Charge,"L");
	  if(TMath::Abs(model)==2) Legend(gr->GetHistogram(),start,end,1,2); 
	}
      else  {if(TMath::Abs(model)==2) gr->SetLineColor(colori[i-start]); else gr->SetLineColor(1);  gr->DrawGraph(k,Time,Charge,"L");}
    }	
 
  return gr;
}

TGraph *MeasureWF::VoltTau(Float_t taus, Float_t taue, Int_t start, Int_t end,Float_t mint, Float_t maxt,Int_t Show,Int_t model,Int_t res, TArrayI exlist)
{
  // Calcultaes rendered integrals above FDV with the trapping time. It scan throug and finds trapping time at which the integral doesn't depend on voltage
  //			Float_t taus    ; Time interval
  //			Float_t taue    ; 
  //			Int_t start     ;  start index
  //			Int_t end       ;  end index
  //			Float_t mint    ; Time window 
  //			Float_t maxt    ; 
  //			Int_t Show      ; show graph
  //			Int_t model     ; if model >= 0 as measured 
  //					  if model < 0 deconvoluted 
  //					  Abs(model) - 0 negative voltage
  //					               > positive voltage 
  //                    Int_t res;        resolution; number of points per ns - default = 1 
  //                    TArrayI exlist  ; list of excluded points = 0

  TGraph *gr,*gr1;
  TCanvas *c1;
  Int_t k=0,i=0,minit,maxit;
  Float_t tau=0,InterSec;
  Float_t *Time=new Float_t [(Int_t)(taue-taus)*res+1];
  Float_t *Slope=new Float_t [(Int_t)(taue-taus)*res+1];
  TH1F *his=new TH1F();
  Float_t StartVoltage=Voltages[start],EndVoltage=Voltages[end];
  //if(model!=0) {StartVoltage=-StartVoltage; EndVoltage=-EndVoltage;}
  TF1 *premica=new TF1("p1","pol1",StartVoltage,EndVoltage);
  Elec *elTCT=new Elec(CAP,50,100e-9);
  {c1=new TCanvas("Canvas1","Canvas",800,600); c1->Draw();c1->SetGrid();};
  //  for(tau=taus;tau<=taue;tau+=1/(Float_t)res) //krambi
  for(tau=taue;tau>=taus;tau-=1/(Float_t)res) //matej
    {
    
      if(model>=0) gr=Tau(tau,start,end,mint,maxt,Show,TMath::Abs(model),exlist);
      if(model<0) gr=Tau(elTCT,tau,start,end,mint,maxt,Show,TMath::Abs(model),exlist);
      c1->Update();
      gr->Fit("p1","RQN");
      Slope[k]=premica->GetParameter(1)*1000;
      Time[k]=tau;
      if(k>1)
	if(Slope[k]*Slope[k-1]<0) 
	    InterSec=(Time[k]-Time[k-1])/(Slope[k]-Slope[k-1])*(-Slope[k-1])+Time[k-1];

      //    printf("tau=%f Slope=%f\n",tau,Slope[k]);
      k++;
      for(int s=0;s<1e7;s++);
      delete gr;
    }

  gr1=new TGraph(k,Time,Slope);
  gr1->Draw("ALP"); 
  gr1->SetTitle("Slope vs. correction time");
  gr1->GetHistogram()->SetXTitle("#tau_{tr}[ns]");
  gr1->GetHistogram()->SetYTitle("Slope [arb.]");
  gr1->GetHistogram()->Draw();	
  gr1->Draw("ALP"); 

  printf("Interpolation of Slope to slope=0 gives %lf\n",InterSec);

  delete premica;
  delete his;
  return gr1;
}


void MeasureWF::EffTau(Int_t start, Int_t end, Float_t mint, Float_t maxt,Int_t eh,Float_t Vdep,Float_t D)
{
  // Same as EffTau, with the array of minimal and maximal fit times given
  Float_t *MinTime=new Float_t [end-start+1];
  Float_t *MaxTime=new Float_t [end-start+1];
  for(Int_t i=start; i<=end; i++) {MaxTime[i-start]=maxt; MinTime[i-start]=mint;}
  EffTau(start,end,MinTime,MaxTime,eh,Vdep,D);
}

void MeasureWF::EffTauComplex(Int_t start, Int_t end, Float_t mint, Float_t maxt,Int_t eh,Float_t Vdep,Float_t D)
{
  // Same as EffTauComplex, with  the array of minimal and maximal fit times given
  Float_t *MinTime=new Float_t [end-start+1];
  Float_t *MaxTime=new Float_t [end-start+1];
  for(Int_t i=start; i<=end; i++) {MaxTime[i-start]=maxt; MinTime[i-start]=mint;}
  EffTauComplex(start,end,MinTime,MaxTime,eh,Vdep,D);
}

void MeasureWF::EffTauSave(Int_t start, Int_t end, Float_t *mint, Float_t *maxt,Int_t eh,Float_t *dt,Int_t startV,Float_t D)
{
  // Determination of the tau with exponential fit to the tail of the measured waveform
  // 		Int_t start  ;  start index
  //		Int_t end    ;  end index
  //		Float_t mint ;  Time window 
  //		Float_t maxt ;  
  //		Int_t eh     ; electrons=0 , holes=1
  //		Float_t D    ; detector thickness in um
  //		Float_t *dt  ; carrier drift tume
  //		Int_t startV ; index of the start voltage
  Int_t i,j;
  Int_t color,cii=0;
  Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  //Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  //Int_t colori[]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
  Double_t tau;
  Char_t v[7],vv[20]={"Neff="};

  TArrayD Neff(end-start+1);
  TArrayD NeffC(end-start+1);
  TArrayD A(end-start+1);

  TH1F *his=new TH1F();
  TF1 *efit=new TF1("efit","expo");
  Double_t mob,factor=1;
  //Double_t NeffDep=Vdep/69.6*1e12;
  Float_t MaxTime=-1e20,MinTime=1e20;
  Elec *elTCT=new Elec(CAP,50,100e-9);
  Float_t maxh=-1e20,NeffAver;;
  for(i=start;i<=end;i++) {maxh=GetHA(GetVoltage(i))->GetMaximum()>maxh?GetHA(GetVoltage(i))->GetMaximum():maxh; }

  for(i=start;i<=end;i++)
    {
      efit->SetRange(mint[i-start],maxt[i-start]);
      MaxTime=MaxTime>maxt[i-start]?MaxTime:maxt[i-start];
      MinTime=MinTime<maxt[i-start]?MinTime:mint[i-start];

      GetHistogram(i,his);
      elTCT->Revpreamp(his,1e9);
      his->Fit("efit","RNQ");
      color=colori[cii++]; 
      his->SetLineColor(color);
      if(i==start) 
	{
	  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
	  TString title="Determination of Neff @ T=";title=title+v; title=title+" C";
	  his->SetTitle(title);
	  his->SetXTitle("t[ns]");
	  his->SetYTitle("I [V/50#Omega]");
	  his->GetXaxis()->SetTitleOffset(1.1);
	  his->GetYaxis()->SetTitleOffset(1.1);
	  his->SetLabelSize(0.040,"X");
	  his->SetLabelSize(0.040,"Y");

	  his->GetXaxis()->SetRange(his->GetXaxis()->FindBin(MinTime-5) , his->GetXaxis()->FindBin(MaxTime*1.3));
	  his->SetMaximum(maxh*1.2); 
	  his->DrawCopy(); efit->DrawCopy("SAME"); 
	  Legend(his,start,end,1,2);
	} else {his->DrawCopy("SAME"); efit->DrawCopy("SAME");}
      //   else {if((i-start)%4==0) {his->DrawCopy("SAME"); efit->DrawCopy("SAME");} }


      //tau=1/TMath::Abs(efit->GetParameter(1))*1e-9;
      tau=1/efit->GetParameter(1)*1e-9;

      Neff[i-start]=(11.7*8.85e-4*TMath::Abs(GetVoltage(i)*dt[i-startV]))/(tau*1.6*D*D);
      A[i-start]=Neff[i-start]*tau;
  
      printf("Voltage=%f :: Tau=%e , Neff=%e \n",GetVoltage(i),tau,Neff[i-start]);

    }
  printf("\n\n Float_t []={");
  for(i=start;i<=end;i++) printf("%e,",Neff[i-start]);
  printf("};\n");

}

void MeasureWF::EffTau(Int_t start, Int_t end, Float_t *mint, Float_t *maxt,Int_t eh,Float_t Vdep,Float_t D)
{
  // Determination of the tau with exponential fit to the tail of the measured waveform
  // 		Int_t start  ;  start index
  //		Int_t end    ;  end index
  //		Float_t mint ;  Time window 
  //		Float_t maxt ;  
  //		Int_t eh     ; electrons=0 , holes=1
  //		Float_t Vdep ; full depletion voltage
  //		Float_t D    ; detector thickness in um
  Int_t i,j;
  Int_t color,cii=0;
  Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  //Int_t colori[]={1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};
  Double_t tau;
  Char_t v[7],vv[20]={"Neff="};
  TArrayD Neff(end-start+1);
  TArrayD TauT(end-start+1);
  TArrayD TauR(end-start+1);
  TH1F *his=new TH1F();
  TF1 *efit=new TF1("efit","expo");
  Double_t mob,factor=1;
  Double_t NeffDep=Vdep/69.6*1e12;
  Float_t MaxTime=-1e20,MinTime=1e20;
  Elec *elTCT=new Elec(CAP,50,100e-9);
  Float_t maxh=-1e20,NeffAver;;
  for(i=start;i<=end;i++) {maxh=GetHA(GetVoltage(i))->GetMaximum()>maxh?GetHA(GetVoltage(i))->GetMaximum():maxh; }

  for(i=start;i<=end;i++)
    {
      efit->SetRange(mint[i-start],maxt[i-start]);
      MaxTime=MaxTime>maxt[i-start]?MaxTime:maxt[i-start];
      MinTime=MinTime<maxt[i-start]?MinTime:mint[i-start];

      factor=TMath::Abs(GetVoltage(i))>Vdep?1:TMath::Sqrt(TMath::Abs(GetVoltage(i))/Vdep);
      mob=Mobility(TMath::Abs(GetVoltage(i))/((D*1e-4)*factor),GetT(i)+273,eh,1e12,1);//eh>0?612:1350;
   
      // GetHistogram(i,his);
      //if(i==start) his->DrawCopy(); else his->DrawCopy("SAME");
      GetHistogram(i,his);
      elTCT->Revpreamp(his,1e9);
      his->Fit("efit","RNQ");
      color=colori[cii++]; 
      his->SetLineColor(color);
      if(i==start) 
	{
	  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
	  TString title="Determination of Neff @ T=";title=title+v; title=title+" C";
	  his->SetXTitle("t[ns]");
	  his->SetYTitle("I [V/50#Omega]");
	  his->GetXaxis()->SetTitleOffset(1.1);
	  his->GetYaxis()->SetTitleOffset(1.1);
	  his->SetLabelSize(0.040,"X");
	  his->SetLabelSize(0.040,"Y");
	  his->SetTitle(title);
	  his->GetXaxis()->SetRange(his->GetXaxis()->FindBin(MinTime-5) , his->GetXaxis()->FindBin(MaxTime*1.3));
	  his->SetMaximum(maxh*1.2); 
	  his->DrawCopy(); efit->DrawCopy("SAME"); 
	  Legend(his,start,end,1,2);
	} else {his->DrawCopy("SAME"); efit->DrawCopy("SAME"); }

      //tau=1/TMath::Abs(efit->GetParameter(1))*1e-9;
      tau=1/efit->GetParameter(1)*1e-9;
      //    Neff[i-start]=(11.7*8.85*1e5/(2*1.6))*TMath::Power(D*tau/mob,2)/TMath::Abs(GetVoltage(i));
      Neff[i-start]=(11.7*8.85e5)/(tau*1.6*mob);
      TauR[i-start]=(11.7*8.85e5)/(NeffDep*1.6*mob);
      TauT[i-start]=eh*TMath::Abs(tau)/(1-TMath::Abs(tau)/TauR[i-start]);
  
      //TauT[i-start]=eh*tau/(1+tau/45e-9);
      //      Neff[i-start]=(11.7*8.85e5)/(TauT[i-start]*1.6*mob);
    
      //    printf("Effective doping concentration ( @ %f V)=%e , tau=%e\n",GetVoltage(i),Neff[i-start],tau);
      printf("Voltage=%f :: TauM=%e , TauTr=%e , TauR=%e , Neff=%e\n",GetVoltage(i),tau,TauT[i-start],TauR[i-start],Neff[i-start]);
      //if(i==start) his->DrawCopy(); else his->DrawCopy("SAME");
      //his->Reset();
    }
  NeffAver=Neff.GetSum()/Neff.GetSize();; 
  printf("Average Neff=%e ---> FDV=%f ::: Average TauTr=%e\n",NeffAver,NeffAver/1e12*69.3,TauT.GetSum()/TauT.GetSize()); 
  NeffAver=NeffAver/1e10;
  int2ascii(&vv[5],NeffAver,0);
  TText *text=new TText((MinTime+MaxTime)/2 , maxh*0.9 ,vv);
  text->Draw();
  //return *Neff;
}



void MeasureWF::EffTauComplex(Int_t start, Int_t end, Float_t *mint, Float_t *maxt,Int_t eh,Float_t Vdep,Float_t D)
{
  // Determination of the tau with exponential fit to the tail of the measured waveform
  // 		Int_t start  ;  start index
  //		Int_t end    ;  end index
  //		Float_t mint ;  Time window 
  //		Float_t maxt ;  
  //		Int_t eh     ; electrons=0 , holes=1
  //		Float_t Vdep ; full depletion voltage
  //		Float_t D    ; detector thickness in um
  Int_t i,j;
  Double_t tau;
  TArrayD TauT(end-start+1);
  TArrayD TauR(end-start+1);
  Float_t Mobil[50];
  Float_t EMobil[50];

  TH1F *his;//=new TH1F();
  TF1 *efit=new TF1("efit","[0]*exp(-x/[1]-(x-[4])/[2])/pow(exp(-(x-[4])/[5])-1+[3],2)");
  TF1 *prem=new TF1("prem","pol1");
  Double_t mob,factor=1;
  Double_t NeffDep=Vdep/69.6*1e12;
  Float_t MaxTime=-1e20,MinTime=1e20;
  Elec *elTCT=new Elec(CAP,50,100e-9);

  Float_t mobconst,mobslope,Emax,Emin,Vol,alpha;
  Double_t par[6];
  //Float_t k=1;
  Int_t sign=eh>0?1:-1;
  TGraph *mobgraph;

  D*=1e-4;

  for(i=start;i<=end;i++)
    {
      his=GetHA(GetVoltage(i));
      //   elTCT->Revpreamp(his,1e9); 
      //maxt[i-start]=his->GetBinCenter();
      efit->SetRange(mint[i-start],maxt[i-start]);
      MaxTime=MaxTime>maxt[i-start]?MaxTime:maxt[i-start];
      MinTime=MinTime<maxt[i-start]?MinTime:mint[i-start];

      Vol=TMath::Abs(GetVoltage(i));
      Emax=(Vol+Vdep)/D;
      Emin=(Vol-Vdep)/D;
      for(j=0;j<50;j++) {EMobil[j]=(Emax-Emin)/j+Emin; Mobil[j]=Mobility(EMobil[j],GetT(i)+273,sign,1e12); }
      mobgraph=new TGraph(50,EMobil,Mobil);
      prem->SetRange(Emin,Emax);
      mobgraph->Fit("prem","RQN");
      mobconst=prem->GetParameter(0);
      mobslope=TMath::Abs(prem->GetParameter(1));
      // printf("Mobility Slope=%f, Mobility Const=%f ",prem->GetParameter(1),mobconst);
      mobslope/=mobconst;
      alpha=(Vol+sign*Vdep)/D;
      TauR[i-start]=(11.7*8.85e5)/(NeffDep*1.6*mobconst);
      // printf("alpha=%f TauR=%e \n",alpha,TauR[i-start]*1e9);
      par[0]=his->GetMaximum();
      par[1]=30;
      par[2]=TauR[i-start]*1e9*sign;
      par[3]=1/(alpha*mobslope);
      if(eh==1 || eh==-1) par[4]=0; else par[4]=mint[i-start];//eh>0?0:MinTime;
      par[5]=par[2];
      efit->SetParLimits(2,-100,-100);
      efit->SetParLimits(3,-100,-100);
      efit->SetParLimits(4,-100,-100);
      efit->SetParLimits(5,-100,-100);
      efit->SetParameters(par);
   
      his->Fit("efit","RQ");  
      tau=TMath::Abs(efit->GetParameter(1))*1e-9;
      TauT[i-start]=tau;
      printf("Voltage=%f :: TauM=%e , TauR=%e par[5]=%f\n",GetVoltage(i),TauT[i-start],TauR[i-start],efit->GetParameter(5));
      //if(i==start) his->DrawCopy(); else his->DrawCopy("SAME");
      //his->Reset();
      delete mobgraph;
    }
  printf("Average trapping time=%e\n",TauT.GetSum()/TauT.GetSize()); 
  DrawMulti(MinTime-5,MaxTime*1.3,start,end,1,2);

}

TH1F *MeasureWF::ElField(Int_t hisnum,Float_t tau,Float_t ts,Float_t te, Int_t nab,Int_t what, Int_t MobMod)
{
  //extraction of the electric field from the measurements
  // 		Int_t hisnum ; number of histogram
  //		Float_t tau  ; effective trapping time
  //		Float_t ts   ; low edge of the time window (drift)
  //		Float_t te   ; high edge of the time window (drift)
  //		Int_t nab    ; charge of the carrier (=1 hole, =-1 electron)
  //            Int_t what   ; what kind of output  histogram
  //		Int_t MobMod ; Mobility Model (see EField Class)

  Float_t D=301;
  Float_t sum=0,sum1=0,vel=0;
  Float_t mindif=1e30;
  Int_t  maxit,minit,mintime=0,maxt,maxt2,maxt1,mint,i=0,nosteps=0,j=0;
  Elec *elTCT=new Elec(CAP,50,100e-9);
  TH1F *his=new TH1F();
  GetHistogram(hisnum,his);
  elTCT->Revpreamp(his,1e9);

  // mint=his->GetXaxis()->FindBin(tis);
  // maxt=his->GetXaxis()->FindBin(tie);
  maxit=his->GetXaxis()->FindBin(te);
  minit=his->GetXaxis()->FindBin(ts);
  TH1F *xt=new TH1F("xvst","x vs. time",maxit-minit,0.,te-ts);
  TH1F *Et=new TH1F("Evst","E vs. time",maxit-minit,0.,te-ts);
  TH1F *Ex=new TH1F("E vs x","Electric field profile",(Int_t) D,0.,D);
  TH1F *ExTemp=new TH1F("E vs x (temp)","Electric field profile (temp)",(Int_t) D,0.,D);
  TF1 *p2=new TF1("polinom2","pol2",0,D);
  Et->SetXTitle("t[ns]"); xt->SetXTitle("t[ns]"); Ex->SetXTitle("depth [ #mum ]");
  Et->SetYTitle("E [V/cm]"); xt->SetYTitle("depth [ #mum ]"); Ex->SetYTitle("E [V/cm]");
  Ex->GetYaxis()->SetTitleOffset(1.3);
  Et->GetYaxis()->SetTitleOffset(1.3);

  printf("Signal Range(%d,%d) :: Carrier=%d \n",minit,maxit,nab);
  for(i=minit;i<maxit;i++)
    {
      sum+=his->GetBinContent(i)*his->GetBinWidth(i)*TMath::Exp((his->GetBinCenter(i)-ts)/tau);
      xt->SetBinContent(i-minit,sum);
      // printf("i=%d, t=%f, sum=%f\n",i-minit,his->GetBinCenter(i)-ts,sum);
    }
 
  xt->Scale(D/sum);
  // for(i=mint;i<maxt;i++)  sum1+=his->GetBinContent(i)*his->GetBinWidth(i)*TMath::Exp((his->GetBinCenter(i)+0.5)/tau);

  printf("Total charge=%f \n",sum);

  Float_t a,b,eps=0,y1,y2,y3,Temp;
  Temp=GetT(hisnum)+273;

  //while(TMath::Abs(sum1-GetVoltage(hisnum)<0.1*TMath::Abs(GetVoltage(hisnum)))
  //{

  mint=his->GetXaxis()->FindBin(ts);
  maxt1=his->GetXaxis()->FindBin(te*0.7);
  maxt2=his->GetXaxis()->FindBin(te*1.4);
  //   maxt=maxt1;
  for(maxt=maxt1;maxt<maxt2;maxt++)
    {
      sum1=0;
      for(i=mint;i<maxt;i++)  //sum1+=his->GetBinContent(i)*his->GetBinWidth(i)*TMath::Exp((his->GetBinCenter(i)+0.5)/tau);
	sum1+=his->GetBinContent(i)*his->GetBinWidth(i)*TMath::Exp((his->GetBinCenter(i)+0.5)/tau);
      //       printf("mint=%f, maxt=%f, sum1=%f :::: ",ts,his->GetBinCenter(maxt),sum1);

      //  while(epsvol>0.01 && nostepsvol<1e3)
      //        {
      //  	for(i=mint;i<aa;i++)  y1vol+=his->GetBinContent(i)*his->GetBinWidth(i)*TMath::Exp((his->GetBinCenter(i)+0.5)/tau);
      //  	for(i=mint;i<bb;i++)  y2vol+=his->GetBinContent(i)*his->GetBinWidth(i)*TMath::Exp((his->GetBinCenter(i)+0.5)/tau);
      //  	for(i=mint;i<(aa+bb)/2;i++)  y3vol+=his->GetBinContent(i)*his->GetBinWidth(i)*TMath::Exp((his->GetBinCenter(i)+0.5)/tau);
      //  	if(y1vol*y2vol>0 && nostepsvol==0) printf("Error somewhere");
	
      for(i=minit;i<maxit;i++)
	{
     
	  vel=his->GetBinContent(i)*TMath::Exp((his->GetBinCenter(i)-ts)/tau)*D/sum1*1e5;
	  a=0;  b=-GetVoltage(hisnum)*1e4;
	  eps=TMath::Abs(a-b);
	  nosteps=0;
	  while(eps>0.01 && nosteps < 1e5)
	    {	
   
	      y1=vel-Mobility(a,Temp,nab,1e12, MobMod)*a;
	      y2=vel-Mobility(b,Temp,nab,1e12, MobMod)*b;
	      y3=vel-Mobility((a+b)/2,Temp,nab,1e12, MobMod)*(a+b)/2;
	      if(y1*y2>0 && nosteps==0) {printf("Velocity to high!!!\n"); b=0; a=0; break;}   
	      //     printf("y1=%f , y2=%f, y3=%e , a=%e, b=%e, T=%f \n",y1,y2,y3,a,b,Temp);
	      if(y1*y3<0) b=(a+b)/2.; else a=(a+b)/2;
	      eps=TMath::Abs(a-b);
	      nosteps++;

	    }
	  //  printf("i=%d, vel=%f, E=%f, eps=%f, nosteps=%d\n",i,vel,(a+b)/2,eps,nosteps);
	  Et->SetBinContent(i-minit,(a+b)/2);
	}

      for(i=0;i<xt->GetNbinsX();i++) ExTemp->Fill(xt->GetBinContent(i),Et->GetBinContent(i));
      ExTemp->Fit("polinom2","RNQ");
    
      if(mindif>(TMath::Abs(GetVoltage(hisnum)+p2->Integral(0,(Int_t)D)*1e-4)))
	{	
	  mindif=TMath::Abs(GetVoltage(hisnum)+p2->Integral(0,(Int_t)D)*1e-4);
	  mintime=maxt;
	  //    printf("mindif=%f (integral=%e) mintime=%f\n",mindif,p2->Integral(0,301)*1e-4,his->GetBinCenter(mintime));
	  Ex->Reset();
	  for(i=0;i<Ex->GetNbinsX();i++) Ex->SetBinContent(i,ExTemp->GetBinContent(i));
	}
      //printf("mindif=%f (integral=%e) mintime=%d\n",mindif,p2->Integral(0,301)*1e-4,mintime);
      ExTemp->Reset(); 
    }

  //for(i=0;i<Ex->GetNbinsX();i++) Ex->SetBinContent(i,ExTemp->GetBinContent(i));
  //for(i=0;i<xt->GetNbinsX();i++) Ex->Fill(xt->GetBinContent(i),Et->GetBinContent(i));
  //TF1 *p2=new TF1("polinom2","pol2",0,301);
  Float_t entryp[3]={25,301,0};
  Float_t exitp[3]={25,280,0};

  TF1 *neff=new TF1("p1","pol0",0,302);
  neff->SetParameter(0,1);
  //Dioda *d=new Dioda(neff,TMath::Abs(GetVoltage(hisnum)));

  switch(what)
    {
    case 0: return(xt); break;
    case 1: return(Et); break;
    case 2: Ex->Fit("polinom2","R");
      if(nab>0)
	{
	  Float_t c=p2->GetParameter(0);
	  Float_t b=p2->GetParameter(1);
	  Float_t a=p2->GetParameter(2);
	  p2->SetParameter(0,a*D*D+b*D+c);
	  p2->SetParameter(1,-2*D*a-b);
	  p2->SetParameter(2,a);	
	}
      printf("a=%e , b=%e, c=%e\n",p2->GetParameter(2),p2->GetParameter(1),p2->GetParameter(0));
      printf("Neff =  %f * x + %f !!!!!!!!!!\n ",2*p2->GetParameter(2),p2->GetParameter(1));
      printf("DEVIATION FROM LINEAR (k*x/2n) = %f !!!!!!!!!!\n ",(2*p2->GetParameter(2)*150)/p2->GetParameter(1));
      printf("Normalization integration from %4.2e to %4.2e\n",his->GetBinCenter(mint),his->GetBinCenter(mintime));
      printf("Function integral=%f (U=%f)\n",p2->Integral(0,301)*1e-4,GetVoltage(hisnum));
      return(Ex); break;
    case 3: 
      //    TF1 *neff=new TF1("p1","pol0",0,301);
      //    neff->SetParameter(0,1);
      Ex->Scale(1e-4);
      Ex->Fit("polinom2","RNQ");
  
      if(nab>0)
	{
	  Float_t c=p2->GetParameter(0);
	  Float_t b=p2->GetParameter(1);
	  Float_t a=p2->GetParameter(2);
	  p2->SetParameter(0,a*D*D+b*D+c);
	  p2->SetParameter(1,-2*D*a-b);
	  p2->SetParameter(2,a);	
	  exitp[1]=21;
	  entryp[1]=1;
	}
      his->DrawCopy("SAME");
      Ex->Scale(1e4);
      //Ex->Fit("polinom2","R");

      printf("a=%e , b=%e, c=%e\n",p2->GetParameter(2)*1e4,p2->GetParameter(1)*1e4,p2->GetParameter(0)*1e4);
      printf("Neff =  %f * x + %f !!!!!!!!!!\n",2*p2->GetParameter(2)*1e4,p2->GetParameter(1)*1e4);
      printf("DEVIATION FROM LINEAR (k*x/2n) = %f !!!!!!!!!!\n ",(2*p2->GetParameter(2)*150)/p2->GetParameter(1));
      printf("Normalization integration from %4.2e to %4.2e\n",his->GetBinCenter(mint),his->GetBinCenter(maxt));
      printf("Function integral=%f (U=%f)\n",p2->Integral(0,301),GetVoltage(hisnum));
      return Ex;
      break;
    default: return(Ex); break;
    }

}

TH1F *MeasureWF::Derivation(Float_t Volt, Int_t nump, Int_t Option)
{
  // Calcultes the charge (current integral) as a function of time
  //				Float_t Volt; voltage
  Int_t index;
  TH1F *his=new TH1F();
  if((index=GetIndex(Volt))==-1) return(his);  
  Float_t sum=0,x1,x2,x3,y1,y2,y3,aa,bb,a,b;
  TH1F *hism=((TH1F *)histo->At(index));
  ((TH1F *)histo->At(index))->Copy(*his);
  Int_t nbins=his->GetNbinsX();
  TF1 *f2=new TF1("f2","pol2",0,1000);

  for(Int_t i=1;i<nbins;i++)
    {
      switch(Option)
	{
    case 0: 
      x1=hism->GetBinCenter(i-1); y1=hism->GetBinContent(i-1);
      x2=hism->GetBinCenter(i);   y2=hism->GetBinContent(i);
      x3=hism->GetBinCenter(i+1); y3=hism->GetBinContent(i+1);
      
      aa=(y2-y1)*(x3*x3-x1*x1)/(x2*x2-x1*x1);
      bb=-aa*(x2-x1)/(y2-y1)+(x3-x1);
      b=((y3-y1)-aa)/bb;
      a=((y2-y1)-b*(x2-x1))/(x2*x2-x1*x1);
      sum=2*a*x2+b;
      break;
    case 1: 
      hism->Fit(f2,"NRQ","",hism->GetBinCenter(i-nump),hism->GetBinCenter(i+nump));
      sum=2*f2->GetParameter(2)*hism->GetBinCenter(i)+ f2->GetParameter(1);;
      break;
	}
      //     sum=(hism->GetBinContent(i+1)-hism->GetBinContent(i-1))/(hism->GetBinCenter(i+1)-hism->GetBinCenter(i-1));
     his->SetBinContent(i,sum);
    }
  his->SetXTitle("t[ns]");
  his->SetYTitle("Charge [arb.]");
  his->SetLabelSize(0.04,"X");
  his->SetLabelSize(0.04,"Y");
  his->SetTitle("Derivative of pulse shape");
  return (his);
}


TH1F *MeasureWF::ChargeShape(Float_t Volt)
{
  // Calcultes the charge (current integral) as a function of time
  //				Float_t Volt; voltage
  Int_t index;
  TH1F *his=new TH1F();
  if((index=GetIndex(Volt))==-1) return(his);  
  Float_t sum=0;
  ((TH1F *)histo->At(index))->Copy(*his);
  Int_t nbins=his->GetNbinsX();
  for(Int_t i=0;i<nbins;i++)
    {
	sum+=his->GetBinContent(i)*his->GetBinWidth(i); 
        his->SetBinContent(i,sum);
    }
  his->SetXTitle("t[ns]");
  his->SetYTitle("Charge [arb.]");
  his->SetLabelSize(0.04,"X");
  his->SetLabelSize(0.04,"Y");
  his->SetTitle("Pulse Shape Integral");
  return (his);
}

void MeasureWF::Derivation(Int_t start, Int_t end, Int_t step)
{
  TH1F *his;
  Int_t i,maxi,mini,color,cii=0; 
  Int_t colori[]={1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,6,7,1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};

  if(start<0) start=0;
  if(end>Multiple) end=Multiple-1;
  for(Int_t i=start;i<end;i+=step)
    {
      
      his=Derivation(GetVoltage(i));
      his->SetLineColor(colori[cii]);
      if(i==start) his->DrawCopy(); else  his->DrawCopy("SAME");
      cii++;
    }
}

void MeasureWF::ChargeShape(Int_t start, Int_t end, Int_t step)
{
  TH1F *his;
  Int_t i,maxi,mini,color,cii=0; 
  Int_t colori[]={1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,6,7,1,2,3,4,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7,1,2,3,4,5,6,7,13,28,30,34,38,40,31,46,49,1,2,3,4,5,6,7};

  if(start<0) start=0;
  if(end>Multiple) end=Multiple-1;
  for(Int_t i=start;i<end;i+=step)
    {
      
      his=ChargeShape(GetVoltage(i));
      his->SetLineColor(colori[cii]);
      if(i==start) his->DrawCopy(); else  his->DrawCopy("SAME");
      cii++;
    }
}



void MeasureWF::CorrectBaseLine(Int_t method)
{
  // Corrects the base line from [-oo,0]. It is a shift in y -axis!
  //	Int_t method   ;   =0 all plots are averaged , =1 separate waveforms are used
  
  Int_t right,left=1;
  Int_t i,j;
  Double_t sum=0,corr=0;
  TH1F *his;

  if(!method)    {	
    for(i=0;i<Multiple;i++)
      {
        // left=((TH1F *)histo->At(i))->GetXaxis()->FindBin(-130.);
	right=((TH1F *)histo->At(i))->GetXaxis()->FindBin(0.);
	sum+=((TH1F *)histo->At(i))->Integral(left,right);
      }
    corr=sum/(Multiple*(right-left));
    //      printf("Baseline correction = %e , Integral before trigger=%e , Nbins=%d!\n",corr,sum,right-left);
  }
  
  for(j=0;j<Multiple;j++){
    if(j==0)  printf("Baseline correction:: ");
      his=((TH1F *)histo->At(j));
    if(method){
      right=his->GetXaxis()->FindBin(0.);
      his->Integral(left,right);
      corr=his->Integral(left,right)/(right-left);
      printf("%d, ",j);
      //printf("%d :: Baseline correction = %e , Integral before trigger=%e , Nbins=%d!\n",j,corr,his->Integral(left,right),right-left);
    }
    for(i=1;i<his->GetNbinsX();i++)
      his->SetBinContent(i,his->GetBinContent(i)-corr);
  } 
  printf("\n"); 
}

/*
  void MeasureWF::ZL2(Int_t start, Int_t end, Float_t mint, Float_t maxt,Int_t eh,Float_t D)
  {
  Int_t i,j;
  Double_t tau;
  TArrayD Neff(end-start+1);
  TH1F *his=new TH1F();
  TF1 *efit=new TF1("efit","expo",mint,maxt);
  Double_t mob=eh>0?480:1350;

  for(i=start;i<=end;i++)
  {
  GetHistogram(i,his);
  his->Fit("efit","RQN");
  tau=efit->GetParameter(1);
  Neff[i-start]=(11.7*8.85*1e5/(2*1.6))*TMath::Power(D*tau/mob,2)/TMath::Abs(GetVoltage(i));
  printf("Effective doping concentration ( @ %f V)=%e , tau=%e\n",GetVoltage(i),Neff[i-start],tau);
  //if(i==start) his->DrawCopy(); else his->DrawCopy("SAME");
  his->Reset();
  }
  printf("Average Neff=%e ---> FDV=%f\n",Neff.GetSum()/Neff.GetSize(),Neff.GetSum()/Neff.GetSize()/1e12*69.3); 
  }
*/
void MeasureWF::Info()
{
  // shows information about measurements
  printf("Mesurement information:\n");
  printf("DATE: %d.%d.%d  TIME: %d:%d:%d\n",(Int_t)Date[0],(Int_t)Date[1],(Int_t)Date[2],(Int_t)Date[3],(Int_t)Date[4],(Int_t)Date[5]);
  printf("Frequency: %4.2f \n",Frequency);
  for(Int_t i=0;i<Multiple;i++) printf(" Temperature=%4.1f  Voltage=%4.1f  Current=%4.2e\n",Temperature[i],Voltages[i],Current[i]);
}

Float_t MeasureWF::GetTime(Float_t scale)
{
  // Calculates the time of measurement
  //		Float_t scale ; scale (3600 = 1h)
  Float_t MonthSec[12]={0,31,59,90,120,151,181,212,243,273,303,333};
  Float_t Year=Date[2]*31536000./scale;
  if(((Int_t)Year)%4==0) for(Int_t i=2;i<12;i++) MonthSec[i]+=1;
  Float_t Month=MonthSec[(Int_t) (Date[1]-1)]*86400./scale;
  Float_t Day=Date[0]*86400./scale;;
  Float_t Hour=Date[3]*3600./scale;
  Float_t Min=Date[4]*60./scale;
  Float_t Sec=Date[5]/scale;
  //printf("%f %f %f %f %f %f\n",Year,Month,Day,Hour,Min,Sec);
  return(Year+Month+Day+Hour+Min+Sec);
}

TGraph *MeasureWF::DrawIV(Int_t model)
{
  // Draws IV for the given measurement
  Char_t v[6];
  Int_t i;
  Float_t *voltages=new Float_t[Multiple];
  Float_t *current=new Float_t[Multiple];
  for(i=0;i<Multiple;i++) 
    {
      if(model==1) {voltages[i]=-Voltages[i]; current[i]=-Current[i]*1e6;}
      else {voltages[i]=Voltages[i]; current[i]=Current[i]*1e6;}
    }
  TGraph *iv=new TGraph(Multiple,voltages,current);
  iv->SetLineWidth(4);
  iv->SetMarkerStyle(21);

  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
  TString title="I-V @ T=";title=title+v; title=title+" C";
  iv->SetTitle((const char *)title); 

  iv->Draw("APL");
  iv->GetHistogram()->SetXTitle("U[V]");
  iv->GetHistogram()->SetYTitle("I[#mu A]");
  iv->GetHistogram()->Draw();
  iv->Draw("APL");
  return(iv);

  iv->Draw("AL*");
}

TGraph *MeasureWF::DrawTV(Int_t model)
{
  // Draws TV for the given measurement
  // Int_t model ; model=0 as measured , model=1  change the sign of both axes 
  Int_t i;
  Float_t *voltages=new Float_t[Multiple];
  Float_t *temperature=new Float_t[Multiple];
  for(i=0;i<Multiple;i++) 
    {
      if(model==1) {voltages[i]=-Voltages[i]; }
      else {voltages[i]=Voltages[i]; }
    }

  TGraph *tv=new TGraph(Multiple,voltages,Temperature.GetArray());
  tv->SetLineWidth(4);
  tv->SetMarkerStyle(21);
  tv->SetTitle("T-V");
  tv->Draw("APL");
  tv->GetHistogram()->SetXTitle("U[V]");
  tv->GetHistogram()->SetYTitle("T[C]");
  tv->GetHistogram()->Draw();
  tv->Draw("APL");
  return(tv);
}


TH1F* MeasureWF::EShaping(Elec *el,Int_t index)
{
  // The waveform is shaped with electronics 
  //		Int_t index; index of the plot
  TH1F *his=new TH1F();
  if(index>Multiple-1 || index<0) { printf("Index out of bounds\n"); return(his);}
  ((TH1F *)histo->At(index))->Copy(*his);
  Double_t xmax=his->GetXaxis()->GetXmax();
  Double_t xmin=his->GetXaxis()->GetXmin();
  his->GetXaxis()->SetLimits(xmin*1e-9,xmax*1e-9);
  el->preamp(his);
  el->CRshape(his);
  el->RCshape(his);
  el->RCshape(his); 
  return(his);
}	


void MeasureWF::NormArray(Int_t num,Float_t *array)
{
  // array normalization
  Float_t max=0;
  Int_t maxi=0,i=0;
  for(i=0;i<num;i++) if(array[i]>max) {max=array[i]; maxi=i;}
  for(i=0;i<num;i++) array[i]/=max;
}

TGraph *MeasureWF::CCEE(Elec *el,Float_t normalize,Int_t model,Int_t Show)
{
  // Same as CCE but electronics is used for shaping (peak of the shaped pulse is used)!
  //			Elec *el          ;  electronics
  //			Float_t normalize ;  normalization to certain number
  //	                Int_t model       ;  lin-lin=0  scale  sqrt-lin=1 abs(lin)-lin=2
  //			Int_t Show        ;  show plots
  Char_t v[6];
  Int_t i=0;
  Float_t *U=new Float_t[Multiple];
  Float_t *CCE=new Float_t[Multiple];
  TH1F *his=new TH1F();
  for(i=0;i<Multiple;i++)
    { 
      ((TH1F *)histo->At(i))->Copy(*his);
      Double_t xmax=his->GetXaxis()->GetXmax();
      Double_t xmin=his->GetXaxis()->GetXmin();
      his->GetXaxis()->SetLimits(xmin*1e-9,xmax*1e-9);
      el->preamp(his);
      el->CRshape(his);
      el->RCshape(his);
      el->RCshape(his); 
      if(model==2) U[i]=TMath::Abs(Voltages[i]); 
      if(model==1) U[i]=TMath::Sqrt(TMath::Abs(Voltages[i]));
      if(model==0) U[i]=Voltages[i];
      CCE[i]= his->GetMaximum();
    }
  NormArray(Multiple,CCE);
  if(normalize!=-1111) for(i=0;i<Multiple;i++) CCE[i]*=normalize;

  TGraph *gr=new TGraph(Multiple,U,CCE);
  gr->SetLineColor(2);
  gr->SetLineWidth(4);
  gr->SetMarkerColor(4);
  gr->SetMarkerStyle(21);
  int2ascii(v,Temperature.GetSum()/Temperature.GetSize());
  TString title="Charge (electronics) @ T=";title=title+v; title=title+" C";
  gr->SetTitle((const char *)title); 

  if(Show)
    {
      gr->Draw("APL");
      if(!model) gr->GetHistogram()->SetXTitle("U[V]"); else gr->GetHistogram()->SetXTitle("Sqrt U[ Sqrt V]");
      gr->GetHistogram()->SetYTitle("CCE");
      gr->GetHistogram()->Draw();
      gr->Draw("APL");
    }
  return(gr);
}


Double_t MeasureWF::Mobility(Float_t E,Float_t T,Float_t Charg,Double_t Neff, Int_t which)
{
  Double_t lfm=0,hfm=0;
  Double_t vsatn,vsatp;
  Double_t betap,betan;
  Double_t alpha;
  switch(which)
    {
    case 0:       
      alpha=0.72*TMath::Power(T/300,0.065);
      if(Charg>0)
	{
	  Double_t ulp=460*TMath::Power(T/300,-2.18);
	  Double_t uminp=45*TMath::Power(T/300,-0.45);      
	  Double_t Crefp=2.23e17*TMath::Power(T/300,3.2);
	  betap=1;
	  vsatp=9.05e6*TMath::Sqrt(TMath::TanH(312/T));
	  lfm=uminp+(ulp-uminp)/(1+TMath::Power(Neff/Crefp,alpha));
	  hfm=2*lfm/(1+TMath::Power(1+TMath::Power(2*lfm*E/vsatp,betap),1/betap));
	}
      else
	{
	  Double_t uln=1430*TMath::Power(T/300,-2); 
	  Double_t uminn=80*TMath::Power(T/300,-0.45);
	  Double_t Crefn=1.12e17*TMath::Power(T/300,3.2);
	  betan=2;      
	  vsatn=1.45e7*TMath::Sqrt(TMath::TanH(155/T));
	  lfm=uminn+(uln-uminn)/(1+TMath::Power(Neff/Crefn,alpha));
	  hfm=2*lfm/(1+TMath::Power(1+TMath::Power(2*lfm*E/vsatn,betan),1/betan));
	}
      break;
    case 1:
      //printf("%e ",par[0]);
      if(Charg>0)
	{
	  lfm=8.54e5*TMath::Power(T,-1.075)*TMath::Exp(1-T/124.);
	  vsatp=1.445e7*TMath::Exp(-T/435.9);
	  betap=2.49*TMath::Exp(-T/270.3);
	  hfm=lfm/TMath::Power(1+TMath::Power(lfm*E/vsatp,1/betap),betap);
	}
      else
	{
	  lfm=2.712e8*TMath::Power(T,-2.133);
	  vsatn=1.586e7*TMath::Exp(-T/723.6);
	  betan=-8.262e-8*TMath::Power(T,3)+6.817e-5*TMath::Power(T,2)-1.847e-2*T+2.429;
	  hfm=lfm/TMath::Power(1+TMath::Power(lfm*E/vsatn,1/betan),betan);
	}
      break;

    }
  return hfm; 
}

Int_t MeasureWF::int2ascii(Char_t v[],Float_t vol,Int_t sign)
{
  // int to ascii with signs (same as in MeasureWF)
  Int_t k=0;
  if(sign) if(vol>0) v[k++]='+'; else {v[k++]='-'; vol=-vol;}
  if(!sign) vol=vol>0?vol:-vol; 
  if((Int_t) vol/100!=0) v[k++]=(Char_t)(((Int_t) vol)/100)+48;
  v[k++]=(Char_t)(((Int_t) vol%100)/10)+48;
  v[k++]=(Char_t)((Int_t) vol%10)+48; 
  v[k++]='\0';
  return k-1;
}

//  void MeasureWF::int2ascii(Char_t v[],Int_t vol)
//  {
//    //  converts int to ascii (3 digit)
//    //		Char_t v[]; array to which the result is stored
//    //		Int_t vol ; number
//         if(vol<0) v[0]=45; else v[0]=43;
//         vol=TMath::Abs(vol);
//         v[1]=(Char_t)(((Int_t) vol)/100)+48;
//         v[2]=(Char_t)(((Int_t) vol%100)/10)+48;
//         v[3]=(Char_t)((Int_t) vol%10)+48; 
//         v[4]='\0';
//  }

/*
  void MeasureWF::Copy(MeasureWF *cp)
  {

  }*/

//
void MeasureWF::SetHistoTime(int ind,Float_t *start)
{
  for(Int_t i=0;i<ind;i++)
      SetHistoTime(i,start[i]);
    
}

void MeasureWF::SetHistoTime(int ind,Float_t start)
{ 
  TClonesArray &entry = *histo;  
  TH1F *his=(TH1F *)entry[ind];
  Float_t minx=his->GetXaxis()->GetBinLowEdge(0)+start;
  Float_t maxx=his->GetXaxis()->GetBinUpEdge(his->GetNbinsX())+start;
  his->SetBins(his->GetNbinsX(),minx,maxx);
} 

TH1F *MeasureWF::GetVelHisto(Float_t ltime,Float_t htime, Int_t mode)
{
  Int_t i=0,j=0,lowb,hib;
  TH1F *his;
  Float_t max,val;
  TH1F *rhis=new TH1F("velocity","velocity",Multiple,Voltages[0],Voltages[Multiple-1]);
  //  Elec *elTCT=new Elec(); //Elec(CAP,50,100e-9);
  TF1 *f1=new TF1("f1","pol1",ltime,htime);
  TF1 *f2=new TF1("f2","pol2",ltime,htime);

  for(i=0; i<Multiple; i++)
    { 
      his=((TH1F *)histo->At(i));
      //      elTCT->Revpreamp(CAP,50,(TH1F *)his,1e9);
      lowb=his->FindBin(ltime);
      hib =his->FindBin(htime);
      max=-1e6;

	  switch(mode)
	    {
	      case 0: for(j=lowb;j<=hib;j++)
      		         {
			   val=TMath::Abs(his->GetBinContent(j)); 	  
			   max=val>max?val:max;
			 }
		break;
	    case 1:        
	      max=TMath::Abs(his->Integral(lowb,hib)); 
	      break;
	    case 2:
	      his->Fit("f1","RNQ");
	      max=f1->GetParameter(1);
	      break;
	    case 3:
	      his->Fit("f2","RNQ");
	      max=2*f2->GetParameter(2)*htime+f2->GetParameter(1);
	      break;
	    case 4:
	      his->Fit("f2","RNQ");
	      max=f2->Eval(htime);
	      break;
	    case 5:
	      his->Fit("f1","RNQ");

              lowb=his->FindBin(ltime);
              hib =his->FindBin(htime+1.5); //peak in max 1.5 ns after hbin

	      for(j=lowb;j<=hib;j++)
      		         {
			   val=TMath::Abs(his->GetBinContent(j)); 	  
			   max=val>max?val:max;
			 }
	      max=f1->GetParameter(1)/max;
	      break;
	    
	   };

	  rhis->SetBinContent(i+1,max);
    }

  return rhis;
}

double MeasureWF::GetElField(TH1F *his,Float_t U, Float_t low,Float_t hi)
{ 
  double eps=1,y1,y2,y3,a,b,sum=0,vel;
  int nosteps=0,ss=0;
  double bias,curU=U;
  TH1F *curhis=new TH1F();


  
  //  bias=GetVelField(curhis,curU,low,hi);
  a=5;
  b=1100;
  eps=TMath::Abs(b-a);
  while(eps>0.1 && nosteps < 1e5)
    {
      his->Copy(*curhis);
      y1=U-GetVelField(curhis,a,low,hi); 
      his->Copy(*curhis);
      y2=U-GetVelField(curhis,b,low,hi); 
      his->Copy(*curhis);
      y3=U-GetVelField(curhis,(a+b)/2.,low,hi);
      if(y1*y2>0 && nosteps==0) {printf("Bias difference too high!!!\n"); b=0; a=0; return -1;}
      printf("no steps = %d :: a = %f b = %f (y1=%f, y2=%f, y3=%f)\n",nosteps,a,b,y1,y2,y3);
      if(y1*y3<0) b=(a+b)/2.; else a=(a+b)/2;
	      eps=TMath::Abs(a-b); 
	      nosteps++;      

    }
  printf("Bias Voltage=%f\n",a);
  curhis->Copy(*his);
  return a;
}

double MeasureWF::GetVelE(double E, double charge)
{
  return( Mobility(E,Temperature[0],charge,1e12, 1)*E);
}


double MeasureWF::GetEVel(double vel, double charge)
{
 double eps=1,y1,y2,y3,a,b,sum=0,max;
  int nosteps=0,ss=0;

          a=0;
          b=100*1e4; //40 V / mikron
          eps=TMath::Abs(b-a);
	  while(eps>0.01 && nosteps < 1e5)
	    {
	      y1=vel-Mobility(a,Temperature[0],charge,1e12, 1)*a;
	      y2=vel-Mobility(b,Temperature[0],charge,1e12, 1)*b;
	      y3=vel-Mobility((a+b)/2,Temperature[0],charge,1e12, 1)*(a+b)/2;
	      if(y1*y2>0 && nosteps==0) { b=0; a=0; return -1;}
	      //     printf("y1=%f , y2=%f, y3=%e , a=%e, b=%e, T=%f \n",y1,y2,y3,a,b,Temp);
	      if(y1*y3<0) b=(a+b)/2.; else a=(a+b)/2;
	      eps=TMath::Abs(a-b); //printf("no steps = %d vel = %f %f\n",nosteps,vel,eps);
	      nosteps++;

 	 
	      //printf("i=%d, vel=%f, E=%f, eps=%f, nosteps=%d\n",ss,vel,(a+b)/2,eps,nosteps);
	    }
	  return a;
}

double MeasureWF::GetVelField(TH1F *his,Float_t U, Float_t low,Float_t hi)
{
  Float_t AverE=U/(hi-low)*1e4;
  Float_t AverVel=Mobility(AverE,Temperature[0],-1,1e12, 1)*AverE+Mobility(AverE,Temperature[0],1,1e12, 1)*AverE;
  Float_t SatVel=Mobility(1e6,Temperature[0],-1,1e12, 1)*1e6+Mobility(1e6,Temperature[0],1,1e12, 1)*1e6;
  Int_t   lowb=his->FindBin(low);
  Int_t   hib =his->FindBin(hi);
  Float_t Aver=his->Integral(lowb,hib)/(Float_t)(hib-lowb);
  his->Scale(AverVel/Aver);
  
  printf("AverE=%e AverVel=%e SatVel=%e Aver=%e ::: Scaling Factor=%e\n",AverE,AverVel,SatVel,Aver,AverVel/Aver);

 double eps=1,y1,y2,y3,a,b,sum=0,vel,max;
  int nosteps=0,ss=0;

 max= his->GetMaximum();
 if(max>SatVel) {his->Scale(SatVel/max*0.99); printf("Velocity exceeds saturation value - Correction made !! Warning!!\n");}
  
  /////////// entering bisection - electric field /////////////////////////////

      for(ss=lowb; ss<=hib; ss++)
	{
          vel=TMath::Abs(his->GetBinContent(ss));
          a=0;
          b=100*1e4; //40 V / mikron
          eps=TMath::Abs(b-a);
	  while(eps>0.01 && nosteps < 1e5)
	    {
	      y1=vel-(Mobility(a,Temperature[0],1,1e12, 1)*a+Mobility(a,Temperature[0],-1,1e12, 1)*a);
	      y2=vel-(Mobility(b,Temperature[0],1,1e12, 1)*b+Mobility(b,Temperature[0],-1,1e12, 1)*b);
	      y3=vel-(Mobility((a+b)/2,Temperature[0],+1,1e12, 1)*(a+b)/2+Mobility((a+b)/2,Temperature[0],-1,1e12, 1)*(a+b)/2);
	      if(y1*y2>0 && nosteps==0) {printf("Velocity to high - location %f (%d)!!!\n",his->FindBin(ss),ss); b=0; a=0; return -1;}
	      //     printf("y1=%f , y2=%f, y3=%e , a=%e, b=%e, T=%f \n",y1,y2,y3,a,b,Temp);
	      if(y1*y3<0) b=(a+b)/2.; else a=(a+b)/2;
	      eps=TMath::Abs(a-b); //printf("no steps = %d vel = %f %f\n",nosteps,vel,eps);
	      nosteps++;

 	 
	      //printf("i=%d, vel=%f, E=%f, eps=%f, nosteps=%d\n",ss,vel,(a+b)/2,eps,nosteps);
	    }
	  //	  efield+=a;
          sum+=a*his->GetBinWidth(ss)*1e-4;
	  his->SetBinContent(ss,a);
	}

      //////////////////////////////////////////////

       for(ss=0;ss<lowb;ss++)  his->SetBinContent(ss,0);
       for(ss=hib+1;ss<his->GetNbinsX();ss++)  his->SetBinContent(ss,0);
  
       return sum;	
       //     printf("Napetost=%f \n",sum);
  
}
