# README #

This repository contains the relevant external software and our macros to analyze the measurements from the TCT setup

### What do we need ###

* root 5.34 or newer
* add the TCTAnalyse lib: Instructions here: http://www.particulars.si/TCTAnalyse/TCTAnalyse-Downloads.html

### Contents ###

* extern: The TCTAnalyse package
* exampleMacros: Usefull test macros