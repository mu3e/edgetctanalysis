{ 
  gSystem->Load("TCTAnalyse.sl"); // load the dll library
	PSTCT meas("../test3", 13,2);                 // load the measurements (filename, scale of the signal at t=0, byte order)
	meas.CorrectBaseLine();
	meas.PrintInfo();                                  // Information about the read data 
	MeasureWF *wf=meas.Projection(0,1,0,0,0,0,0,11); 
	wf->DrawTest(1);
	//wf->PrintVoltages();
	wf.DrawMulti(0,100000,0,10,1,0,0);

}
