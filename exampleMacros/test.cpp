#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include <Math/Point2D.h>
#include <Math/Point3D.h>
#include "TCTScan.h"
#include "TMath.h"
#include "stdio.h"
#include <vector>
#include <iostream>
#include "TGraph.h"
//#include "../../extern/TCTAnalyse/TCTScan.h"
using namespace std;

//This macro is useful to analyse the data collected by the TCT labview software (PSTCT) ONLY!
//One can find out the TOT for square wave signals,
//which can be fed in more than one channels of oscilloscope.
//It also gives the latency (time lag) between the two signals.
//Also useful for ploting the Latency and TOT at (x,y) of the detector.
//Contains many more functions to analyse TCT data.

/*            falling              rising
 *             edge                 edge
 *ch1 ___________<-----TOT--------->_____________<-----TOT-------->__________......
 *               |                  |            |                  |
 *               |                  |            |                  |
 *               |__________________|            |__________________|
 *
 *ch2____________________<-----TOT----->_____________<-----TOT----->__________......
 *                       |              |            |              |
 *                       |              |            |              |
 *                       |______________|            |______________|
 *               <--Lat.->
*/





ROOT::Math::XYPoint findExtremums(TH1F * h)  //Returns the max and min values of a histogram
{                                           //(x,y) == (min,max)
    //    cout << h->GetEntries() << endl;
    int max = h->GetBinContent(1000);
    int min = h->GetBinContent(1000);
    int maxBin, minBin;
    for(int i =20;i<h->GetNbinsX();i++)
    {
        int t = h->GetBinContent(i);

        if(t<min)
        {
            minBin = i;
            min=t;
        }
        if(t>max)
        {
            maxBin = i;
            max=t;
        }
    }
    //  cout << "minBin: " << minBin << "\t" << "maxBin: " << maxBin
    //    << "\t" << "Total bins: " << h->GetNbinsX() << endl;
    ROOT::Math::XYPoint tmp(min,max);
    return tmp;
}






int main(){

    string dummy= "/home/lhuth/test9_2017.tct";
    PSTCT stct((char*)dummy.c_str(),0,2);
    stct.PrintInfo();
    TCanvas *c1 = new TCanvas();
    TH1F*j = new TH1F();
//    MeasureWF * wf =stct.Projection(1,1,1,1,0,0,0,19);
//    wf->Draw(0,"colz",1,1);
    j = stct.GetHA(1,stct.indx(0,0,0,0,0));
    j->Draw();
    c1->SaveAs("test.pdf");
}

